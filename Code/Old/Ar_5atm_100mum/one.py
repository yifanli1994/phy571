﻿import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation
from scipy.integrate import odeint
import pylab as pl
from scipy import fftpack


# Physcial parameters 
pi = np.pi
c = 3E8                                    # the velocity of light (m*s^-1)
e = -1.60E-19                              # charge of electron (C)
m = 9.11E-31                               # mass of electron (kg)
epsilon0 = 8.85E-12                        # vacuum permittivity (F*m^−1) 
Ni = 1.34E26                               # densiy of ion (m^-3)
lamb = 620E-9                              # wavelength (m) 
f = c/lamb                                 # frequency

I0 = 1E20                                  # intensity (W/m^2)
E0  = ((1E20)*377)**0.5                    # the amplitude 
omega0 =(2*pi*c)/lamb                      # the angular frequency
t_FWHM = 100E-15                           # half of the W
t0= 200E-15                                # the center of the gaussian wacepacket
n0 = 1
v = c/n0
k0 = omega0/v

#####################################################################

fac = Ni/(4*pi*epsilon0)


#####################################################################
#                                                                   #
#                       Gaussian function                           #
#                                                                   #
#####################################################################

# Gaussian temporal pulse
def gaussian_wavepacket(E0, omega0, t_FWHM, t, t0):
    """The laser intensity is Gaussian temporal pulse
    
    Parameters:
    ----------
    E0 : Amplitude of electric field
    omega0 : Frequency of the source laser
    t_FWHM : full width at half maximun FWHM
    t0 : the center of the gaussian wavepacket
    
    Return:
    ------
    E : electric field of guassia wavepacket
    """
    
    delta_t = t_FWHM/(2*np.sqrt(np.log(2)))
    # The electric field
    E = E0 * np.cos(omega0*(t-t0)) * np.exp(-((t-t0)**2)/(2*delta_t**2))
    return E 

# Propagation of the pulse along with x
def distribution(E0, k0,  x_FWHM, x, x0):
    """The propagation of the pulse along the position x
    
    Parameters:
    ----------
    E0 : Amplitude of electric field
    omega0 : Frequency of the source laser
    x_FWHM : full width at half maximun FWHM
    t0 : the center of the gaussian wavepacket
    
    Return:
    ------
    E : electric field as a fonction of pposition
    """
    # velocity of the propagation and vector 

    delta_x = x_FWHM/(2*np.sqrt(np.log(2)))
    
    # the electric field
    E = E0 * np.cos(k0*(x-x0)) * np.exp(-((x-x0)**2)/(2*delta_x**2))
    return E

# Time average
def average_intensity(E, t, DT):
    """The time average intensity of the electric field 

    Parameters:
    ----------
    E: the electronic field
    t: the time
    DT: the resolution of the time average
    
    Return:
    ------
    I : time average intensiy
    """
    dt = t[1]-t[0]
    n_DT = int(0.5*DT/dt)

    # the average intensity
    temp = np.zeros(len(t))
    I = np.zeros(len(t))
    for i in range(len(t)):
        if i<=n_DT | i>=len(t)-n_DT:
            continue
        for j in range(n_DT):
            temp[i] += E[i+j]*E[i+j]
            temp[i] += E[i-j]*E[i-j]
        I[i] = (1./(2*dt*n_DT))*temp[i]
    return I

# Guassian
def gaussian(A0, x, x_FWHM, x0):
    """The gaussian function
    
    Parameters :
    ----------
    A0 : the amplitude 
    x : the x-axis
    x_FWHM : full width at half maximun FWHM
    x0 : the center
    """
    # variation
    delta_x = x_FWHM/(2*np.sqrt(np.log(2)))
    # gaussian function
    g = A0*np.exp(-((x-x0)**2)/(2*(delta_x**2)))
    return g

# Fourier transform and the spectrum of frequency 
def frequency(f):
    """
    Parameters:
    ----------
    f : the electric field
    
    Return :
    -------
    g : the spectrum of frequency 
    """
    g = np.fft.fft(f)
    return g

# Envelope of wave
def envelope(E):
    """
    Parameters:
    ----------
    E : the electric field
    
    Return :
    -------
    g : the envelope 
    """
    
    he = fftpack.hilbert(E)
    return np.sqrt(E**2 + he**2)

# Electric field propagation equation
def e_propagation_dis(E0, w0, r, z, k, lamb):

    z_r = pi*w0**2/lamb
    
    w_z = w0*np.sqrt(1+(z/z_r)**2)

    r_z = z*(1+(z_r/z)**2)

    E = E0*(w0/w_z)*np.exp(-r**2/(w_z**2))*cos(k*(z+r**2/(2*r_z**2)))
    
    return E

# Intensity propagation equation
def i_propagation_dis(I0, w0, r, z, k, lamb):
    
    z_r = pi*w0**2/lamb
    
    w_z = w0*np.sqrt(1+(z/z_r)**2)


    I = I0*(w0/w_z)**2*np.exp(-2*(r**2)/(w_z**2))
    
    return I

#####################################################################
#                                                                   #
#                         Matplotlib                                #
#                                                                   #
#####################################################################

# two scale 
def two_scales(ax1, xaxis, data1, data2, c1, c2, title, lab1, lab2, xlabel, ylabel1, ylabel2):
    """

    Parameters:
    ----------
    ax : Axis to put two scales on
    xaxis : x-axis values for both datasets
    data1 : Data for left hand scale
    data2 : Data for right hand scale
    c1 : Color for line 1
    c2 : Color for line 2
    title : title of 
    lab1 :  label of 1 scale
    lab2 :  label of 2 scale
    xlabel :  xlabel 
    ylabel1 : ylabel of 1 scale 
    ylabel2 : ylabel of 2 scale 

    Returns:
    -------
    ax : Original axis
    ax2 : New twin axis
    """
    ax2 = ax1.twinx()
    
    ax1.set_title(title, fontsize=35, fontname ='Times New Roman')
    ax1.plot(xaxis, data1, lw=1, color=c1, label=lab1)
    ax1.set_xlabel(xlabel, fontsize=25, fontname ='Times New Roman')
    ax1.set_ylabel(ylabel1, fontsize=25, fontname ='Times New Roman')
    ax2.plot(xaxis, data2,'--', lw=1, color=c2, label=lab2)
    ax2.set_ylabel(ylabel2, fontsize=25, fontname ='Times New Roman')
    ax1.legend()
    ax2.legend()
    return ax1, ax2

# one scale
def one_scale(ax, xaxis, data, c, type_line, title, lab, xlabel, ylabel):
    """

    Parameters:
    ----------
    ax : Axis to put the scale on
    xaxis : x-axis values for the data
    data1 : Data for the scale
    c1 : Color for line 
    title :
    lab :  label of scale
    xlabel :  label of x axis
    ylabel :  label of y axis
    
    Returns:
    -------
    ax : Original axis
    """
    ax.plot(xaxis, data,type_line, lw=1, color=c, label=lab)
    ax.set_title(title, fontsize=35, fontname ='Times New Roman')
    ax.set_xlabel(xlabel, fontsize=25, fontname ='Times New Roman')
    ax.set_ylabel(ylabel, fontsize=25, fontname ='Times New Roman')
    ax.legend()
    return ax

#####################################################################
#                                                                   #
#                    ODE equation(scipy)                            #
#                                                                   #
#####################################################################

# solve the ODE equation dy/dt = -k*y
def ode_0(t, k, y0):
    """Solve the ODE with form dy/dt = -k*y
    
    Parameters:
    ---------
    t : variable
    k : the factor
    y0 : initial condition of the first variable
    
    Return :
    -------
    y : the solution under initial condition 
    """
    # function that returns dy/dt
    def model(y,t):
        dydt = -k*y
        return dydt
    y = odeint(model, y0, t)
    return y

# solve the ODE equation dy(t)/dt = -k*y(t) +h*g(t)   
def ode_j(t, g, k, h, y0):
    """Solve the ODE with form dy(t)/dt = -k*y(t) +h*g(t) 
    
    Parameters:
    ---------
    t : variable
    g : a term varing with variable
    k : factor of term y(t)
    h : factor of term g(t)
    y0 : initial condition of the first variable
    
    Return :
    -------
    y : the solution under initial condition 
    """
    # function that returns dy/dt
    def model(y,t):
        dydt = -k*y + h*g
        return dydt

    # solve ODE
    y = odeint(model, y0, t)
    return y

#####################################################################
#                                                                   #
#                  class including properties                       #
#                                                                   #
#####################################################################

class inter():
    """ This class has the whole properties of the interaction of laser and the medium.
        'field_e', 'current', 'index_f', 'density_e', 'deg_ion', 'der_degree'
    """
    
    def __init__(self, ini_gau, ini_dis, ini_gau_left, ini_dis_left, L, T, N, M):
        """
        Parameters:
        ----------
        int_gau : the initial guassian temporal pulse of right light
        ini_dis : the initial guassian distribution of right light
        ini_gau_left : the initial guassian temporal pulse of left light
        ini_dis_left : the initial guassian distribution of left light
        L : the length of interaction 
        T : the time of propagation
        N : the number of the grid position 
        M : the number of the grid time
        """
        self.ini_gau = ini_gau                   # initial temporal pulse of right light 
        self.ini_dis = ini_dis                   # initial distribution of pulse of right light
        self.ini_gau_left = ini_gau_left         # initial temporal pulse of left light
        self.ini_dis_left = ini_dis_left         # initial distribution of pulse of left light
        self.L = L 
        self.T = T
        self.N = N 
        self.M = M 
        
        self.t_pts, self.dt = np.linspace(0, T, M ,retstep=True)
        self.x_pts, self.dx = np.linspace(0, L, N, retstep=True)

        # initialize these proerties 
        self.e_right = np.zeros((M, N))           # the electric field propagate in the right
        self.e_left = np.zeros((M, N))            # the electric field propagate in the left
        self.j = np.zeros((M, N))                 # the current j
        self.n = np.ones((M, N))                  # the refraction index n
        self.dens_e = np.zeros((M, N))            # the density of electrons
        self.deg_ion = np.zeros((M, N))           # the density of electrons
        self.e = self.e_right + self.e_left       # the total electric field
        
        # initial condition
        self.e_right[:, 0] = self.ini_gau
        self.e_right[0, :] = self.ini_dis
        self.e_left[:, N-1] = self.ini_gau_left
        self.e_left[0, :] = self.ini_dis_left
        self.j[0, :] = 0 
        
    def propagation(self, dens_e):
        """This function updates the data of matrix according to time and position
        """
        self.dens_e[:, 0] = dens_e
        
        for j in range(1, N):
            for i in range(M-1, 1, -1):
                self.dens_e[i, j] = self.dens_e[i-1, j-1]
                
        for i in range(M-1):
            
            # update transverse current j, dt is enough tiny, so we can consider that j(t+dt/2)=j(t)
            # update j(t+dt/2) using the electric field E(t)
            self.j[i+1, :] = self.j[i, :] + self.dt*((e**2)/m)*self.dens_e[i, :]*(self.e_right[i, :]+self.e_right[i, :])
    
            # update electric field in two directions
            for j in range(0, N-1):
                self.e_right[i+1, j+1] = self.e_right[i, j] - pi*self.dt*(self.j[i+1, j] + self.j[i+1,j+1])
    
            for j in range(N-2, 0, -1):  
                self.e_left[i+1, j] = self.e_left[i, j+1] - pi*self.dt*(self.j[i+1, j] + self.j[i+1,j+1])
        # update 
        self.e = self.e_right + self.e_left
 
# the adiabaticity parameter 
def adiabaticity(U_i, E0, omega0):
    """Adiabaticity parameter 
    
    Parameters : 
    ----------
    U_i : the ionization potential 
    E0 : amplitude of electric field
    omega0 : the frequency of laser
    
    Return :
    -------
    adi : the adiabaticity parameter  
    """
    #quiver energy
    U_q = ((e**2)*(E0**2))/(4*m*(omega0**2))
    
    adi = (U_i/(2*U_q))**0.5

    return adi

#####################################################################
#                                                                   #
#                 Ammosov et al. field-ionization                   #
#                                                                   #
#####################################################################

# the Ammosov et al. field-ionization
def ammosov(Z, omega0, U_i, E):
    """Ammosov et al. formula
    This formula is in better agreement with data.
    
    Parameters : 
    ----------
        Z : the residual charge seen by this electron
        U_i : the ionization  
        E : electric field
     
    Return :
    -------
    W : fidld-ionization .   
    """
    
    omega_au = 4.1E16                         # Atomic unit of frequency (s^-1)   
    E_au = 5.14E11                            # Atomic unit of electric field  (V*m^-1)    
    U_H = 13.6                                # The ionization potential of Hydrogen (eV)    
    n_eff = Z/((U_i/U_H)**0.5)                # The effective quantum number 

    # The ionization rate
    
    fac1 = np.exp(-(2/3.) * ((Z**3)/(n_eff**3)) * (E_au/E))
    fac2 = (Z**2/n_eff**4.5) * ((10.87 * (Z**3/n_eff**4) * (E_au/E))**(2*n_eff-1.5))

    W = 1.61*omega_au*fac2*fac1
    
    return W

def ammosov2(Z, omega0, U_i, E, l, m):
    """Ammosov et al. formula provided by second paper
    
    Parameters : 
    ----------
        Z : the residual charge seen by this electron
        U_i : the ionization  
        E : electric field
     
    Return :
    -------
    W : fidld-ionization .   
    """
    
    omega_au = 4.1E16                        # Atomic unit of frequency (s^-1)   
    E_au = 5.14E11                           # Atomic unit of electric field  (V*m^-1)   
    U_H = 13.6                               # The ionization potential of Hydrogen (eV)   
    n_eff = Z/((U_i/U_H)**0.5)               # The effective quantum number     
    eu = 2.7128                              # Euler's number                          

    C_n = ((2*eu/n_eff)**n_eff)*((2*pi*n_eff)**(-0.5))
    
    term1 = ((2*l+1) * np.math.factorial(l+np.abs(m)))/((2**(np.abs(m))) * (np.math.factorial(np.abs(m))) *(np.math.factorial(l-np.abs(m))))
    
    term2 = (2 * ((U_i/U_H)**(3/2)) * (E_au/E))**(2*n_eff-np.abs(m)-1)
    
    term3 = np.exp((-2/3) * ((U_i/U_H)**(3/2)) * (E_au/E))
    
    W_st = 0.5*omega_au*(C_n**2)*(U_i/U_H)*term1*term2*term3
    
    # The mean ionization rate in a sinusoidal field
    W = ((3/pi) * ((U_i/U_H)**(3/2)) * (E_au/E))**0.5 * W_st
    
    return W


#####################################################################
#                                                                   #
#                       shift wavelength                            #
#                                                                   #
#####################################################################

def cross_section(i, E_e, N, P, q):
    """The cross section for collisional ionization from charge state i to charge state i+1
    
    Parameters:
    ----------
    E_e : electron energy
    N : number of the shell of electrons
    P : the ionization potential for the outer outer shell
    q : the number of electrons in each shell 
    
    Return:
    ------
    cross : cross section for collisional ionization
    """
    
    a = 9E-14     # tacke into account any effects caused by collisional ionization of excited state species
    b = c = 0
    sigma = 0 
    
    # the cross section fron i to i+1
    for j in range(N):
        sigma += (a*q[i][j])/(E_e*P[i][j]) * np.log(E_e/P[i][j]) * (1-b*np.exp(-c*(E_e/P[i][j]-1)))
    
    cross = sigma
    return cross

def correction_average(f, D):
    
    # the confocal parameter 
    z_c = 8*lam*f**2/(pi*D**2)
    return z_c

# the delta lambda as a function of density of electron
def delta_lamda_dens(L, lam, der_dens):
    """The delta lambda according to the Drude model and using the temporal derivative of electron densiy 
    Parameters:
    ----------
    L : interaction length 
    lam : wavelength lambda
    der_dens : derivative of electron density
    
    Return:
    ------
    delta_lam : the shift of the wavelength
    """
    
    delta_lam = -((e**2 * L * lam**3)/(2*pi*m*c**3))*der_dens
    return delta_lam

# the delta lambda as a function of dz/dt
def delta_lamda_deg(L, lam, Ni, der_deg):
    """The delta lambda according to the Drude model and using the temporal derivative of the degree of ionization 
    
    Parameters:
    ----------
    L : interaction length 
    lam : wavelength lambda
    Ni : ion density
    der_deg : derivative of the degree of ionization
    
    Return:
    ------
    delta_lam : the shift of the wavelength
    """
    
    delta_lam = -(((e**2)*L*(lam**3)*Ni)/(8*pi*m*epsilon0*(c**3)))*der_deg

    return delta_lam

# the factor of delta lambda as a function of lambda^3
def factor_delta_lamda_deg(L, lam, Ni, der_deg):
    """The delta lambda according to the Drude model and using the temporal derivative of the degree of ionization 
    
    Parameters:
    ----------
    L : interaction length 
    lam : wavelength lambda
    Ni : ion density
    der_deg : derivative of the degree of ionization
    
    Return:
    ------
    delta_lam : the shift of the wavelength
    """
    
    delta_lam = -(((e**2)*L*(lam**3)*Ni)/(8*pi*m*epsilon0*(c**3)))

    return delta_lam

# change a frequency to wavelength
def fre_to_lam(freq_pts, dis):
    """Change the grid time to a grid frequency in a Fourier transform, then change into a grid of wavelength
    
    Parameters:
    ----------
    fre_pts :  frequency 3E14 3E15 
    
    Return :
    ------
    lam_pts : an array of wavelength 
    """
    lamb_pts = np.zeros(1)
    dis_lamb = np.zeros(1)
    
    for i in range(len(freq_pts)):
        if (freq_pts[i]>=3E14)&(freq_pts[i]<=3E15):
            lamb_pts = np.append(lamb_pts, v/freq_pts[i])
            dis_lamb = np.append(dis_lamb, dis[i])
        else: continue    
    np.delete(lamb_pts,[0])
    np.delete(dis_lamb,[0])
    
    return dis_lamb
    
def fre_to_lam_pts(freq_pts,dis):
    """Change the grid time to a grid frequency in a Fourier transform, then change into a grid of wavelength
    
    Parameters:
    ----------
    fre_pts : frequency 3E14 3E15 
    
    Return :
    ------
    lam_pts : an array of wavelength 
    """
    lamb_pts = np.zeros(1)
    dis_lamb = np.zeros(1)
    
    for i in range(len(freq_pts)):
        if (freq_pts[i]>=3E14)&(freq_pts[i]<=3E15):
            lamb_pts = np.append(lamb_pts, v/freq_pts[i])
            dis_lamb = np.append(dis_lamb, dis[i])
        else: continue

    np.delete(lamb_pts,[0])
    np.delete(dis_lamb,[0])
    
    return lamb_pts
    

    
def shift_lam(lam_pts, dis, Ni, der, L):
    """The blueshift of wavelength under the condition of ionization, return an array(length=2*N)
    
    Parameters:
    ----------
    lam_pts : an array of wavelength
    dis_fre : distribution of wavelength corresponding to the grid wavelength
    Ni : ion density related with gas pressure
    der : derivative of the degree of ionization
    L : length of interaction 
    
    Return:
    ------
    couple : a coupled array of corresponding wavelength and distribution
    """
    N = len(lam_pts)

    
    delta_lam = delta_lamda_deg(L, lam_pts, Ni, der)
    
    lam_new = lam_pts + delta_lam
    
    couple = np.zeros((2, N))
    
    couple[0, :] = lam_new
    couple[1, :] = dis

    return couple

#####################################################################
#                                                                   #
#                      Profile of laser                             #
#                                                                   #
#####################################################################

lamb_1 = 2                                  # the wavelenght  
E0_1  = 1.                                  # the amplitude 
omega0_1 =(2*pi*1)/lamb_1                   # the angular frequency
t_FWHM_1 = 6                                # the FWHM     
t0_1= 25                                    # the center of the gaussian wacepacket
DT_1= 0.1                                   # the resolution of the time average 

Nt_1 =1000
tmin_1, tmax_1 = 0, 50
t_pts_1, dt_1 = np.linspace(tmin_1, tmax_1, Nt_1, retstep=True)

# calculer the field 
E_1 = gaussian_wavepacket(E0_1, omega0_1, t_FWHM_1, t_pts_1, t0_1)
I_1 = gaussian(E0_1 , t_pts_1, t_FWHM_1, t0_1)

# plot the laser
fig, ax = plt.subplots(figsize=(14,9))
ax1, ax2 = two_scales(ax, t_pts_1, E_1, I_1, 'b', 'r', 'Gaussian temporal pulse', 'Electric field E(t)', 'Electric field I(t)', 'Time (fs)', 'Amplitude(V/m)', 'Intensity($W/m^{2}$)')
plt.savefig("theoretical gaussian temporal pulse")
plt.show()

#####################################################################
#                                                                   #
#                  propagation in vacuum                            #
#                                                                   #
#####################################################################
# propagation in vacuum

# the initial condition
n = 1                                      # don't consider fractive index 
v = c/n
L = 100E-6                                 # the length of interaction in one dimension  

N = 1000                                  
x_pts, dx = np.linspace(0, L, N, retstep=True)

dt = dx/c                                  # make sure that dt is about 1fs
M = 3000                                   # choose this number to make sure that a pulse pass through the length 
T = (M-1)*dt                               # the considering time 
t_pts = np.linspace(0, T, M)
dens_e0 = np.zeros(M)                       # electron density is zero in vacuum 

print('dx = ', dx)
               
print('dt = ', dt*1E15)

print('time = ', T*1E15)

print('length = ', L)
# the gird position and time with the unit um and fs
t_pts_fs, dt_fs = np.linspace(0, T*1E15, M, retstep=True)
x_pts_um, dx_um = np.linspace(0, L*1E6, N, retstep=True)

# the  initial pulse and distribution
t_FWHM = 100E-15
x_FWHM = v*t_FWHM
t0 = 200E-15
x0 = -v*t0

ini_gau = gaussian_wavepacket(E0, omega0, t_FWHM, t_pts, t0)
ini_dis = distribution(E0, k0, x_FWHM, x_pts, x0)

#####################################################################

# instantiation of the class properties  
pro = inter(ini_gau, ini_dis,0 ,0 , L, T, N, M)

# make laser propagate 
pro.propagation(dens_e0)

fig = plt.figure(figsize=(15,25))
axes = plt.subplot(111)
axes.set_yticks([])
axes.plot(x_pts_um, pro.e_right[300,:], color='royalblue')
axes.plot(x_pts_um, pro.e_right[600,:]+2.5E11, color='royalblue')
axes.plot(x_pts_um, pro.e_right[900,:]+5E11, color='royalblue')
axes.plot(x_pts_um, pro.e_right[1200,:]+7.5E11, color='royalblue')
axes.plot(x_pts_um, pro.e_right[1500,:]+10E11, color='royalblue')
axes.set_xlim(0, 100)
axes.set_title('Propagation along with x', fontsize=35, fontname ='Times New Roman')
axes.set_xlabel('Position($\mu m$)', fontsize=25, fontname ='Times New Roman')
axes.set_ylabel('Discrete time point', fontsize=25, fontname ='Times New Roman')
plt.savefig("propagation_with_time")
plt.legend()
plt.show()

####################################################################
# Diagramme of a gaussian wavepacket
# Use the initial condition at the point x=0,  ini_gau = gaussian_wavepacket(E0, omega0, t_FWHM, t_pts, t0)

E_t = ini_gau

I_t = ((np.abs(E_t))**2 )/377 

# plot the ultrashort laser
fig, ax = plt.subplots(figsize=(14,9))
ax1, ax2 = two_scales(ax, t_pts_fs,  ini_gau, I_t, 'b', 'r', 'Gaussian temporal pulse', '$Electric\ field\ E(t)$', '$Time average intensity I(t)$', 'Time (fs)', 'Amplitude($V/m$)', 'Intensity(relative value)')
ax1.set_xlim(0,400)
plt.legend('best')
plt.savefig("real_gaussian_temporal_pulse")
plt.show()

####################################################################
# Diagramme of a gaussian wavepacket
# Use the initial condition at the point x=0,  ini_gau = gaussian_wavepacket(E0, omega0, t_FWHM, t_pts, t0)

E_t = ini_gau

I_t = gaussian(I0,t_pts,t_FWHM,t0)

# plot the ultrashort laser
fig, ax = plt.subplots(figsize=(14,9))
ax1, ax2 = two_scales(ax, t_pts_fs,  ini_gau, I_t, 'b', 'r', 'Gaussian temporal pulse', '$Electric\ field\ E(t)$', '$Time average intensity I(t)$', 'Time (fs)', 'Amplitude($V/m$)', 'Intensity(relative value)')
ax1.set_xlim(0,400)
plt.legend('best')
plt.savefig("real_gaussian_temporal_pulse_2")
plt.show()

####################################################################
# propagation along with x


x_FWHM = v*t_FWHM
x0 =v*t0
Nx = 1000
xmin, xmax, = 0, 100E-6
x_pts, dx = np.linspace(xmin, xmax, Nx, retstep=True)
xmin_um, xmax_um, = 0, xmax*1E6
x_pts_um, dx_um = np.linspace(xmin_um, xmax_um, Nx, retstep=True)

# calculer the field 
E_x = distribution(E0, k0, x_FWHM, x_pts, x0)
I_x = ((np.abs(E_x))**2 )/377           # feactor is to reach the 10^16W/cm^2

# plot the ultrashort laser
fig, ax = plt.subplots(figsize=(14,9))
ax1, ax2 = two_scales(ax, x_pts_um,  E_x, I_x, 'b', 'r', 'Gaussian spatial pulse', 'E(t)', 'I(t)','Position ($\mu m$)', 'Amplitude($V/m$)', 'Intensity($W/m^{2}$)')
ax1.set_xlim(0,100)

plt.legend('best')
plt.savefig("electric field and intensity(space spectrum)")
plt.show()
####################################################################
# frequency spectrum

# the grid point of frequency
Nt = M 
freq_pts = np.fft.fftfreq(Nt, dt)

# the frequency spectrum
dis_freq = frequency(E_t)
freq_abs = np.abs(dis_freq)
freq_real = np.real(dis_freq)
freq_imag = np.imag(dis_freq)
dis_freq_vacuum =freq_abs


print('dfreq = ', freq_pts[1]- freq_pts[0])
# plot the spectrum of frequency 
fig, axes = plt.subplots(ncols=3, figsize=(20, 11))
ax = axes[0]
ax = one_scale(ax, t_pts_fs, E_t,'royalblue', '-', 'Temporal pulse', 'E(t)', 'Time (fs)', 'Electric filed(V/m)')
ax.set_xlim(0,400)

ax = axes[1]
ax = one_scale(ax, freq_pts, freq_abs, 'g', '-', 'Frequency spectrum', 'P(f)', 'Frequency f (Hz)', 'Itensity')
ax.set_xlim(-0.55E15,0.55E15)

ax = axes[2]
ax = one_scale(ax, freq_pts, freq_abs, 'g', '-', 'Frequency spectrum(positive)', 'P(f)', 'Frequency f (Hz)', 'Intensity')
ax.set_xlim(0.47E15,0.50E15)

plt.savefig("frequency_spectrum")
plt.legend()
plt.show()

#####################################################################

omega_pts = 2*pi*freq_pts

fig = plt.figure(figsize=(15,9))
ax = plt.subplot()
ax = one_scale(ax, omega_pts, freq_abs,'b', '-', 'Frequency spectrum(positive)', 'P($\omega$)', 'Frequency $\omega$ (rad/s)', 'Intensity')
ax.set_xlim(0.25E16,0.35E16)
plt.savefig("frequency_omega_spectrum")
plt.legend()
plt.show()

#####################################################################
# animation of the propagation

fig, ax = plt.subplots(figsize=(15,15))

line, = ax.plot(x_pts_um, pro.e_right[0,:])

def animate(i):
    line.set_ydata(pro.e_right[i,:])  # update the data
    return line,


# Init only required for blitting to give a clean slate.
def init():
    line.set_ydata(pro.e_right[0,:])
    return line,

# call the animator.  blit=True means only re-draw the parts that have changed.
# blit=True dose not work on Mac, set blit=False
# interval= update frequency
ani = animation.FuncAnimation(fig=fig, func=animate, frames=100, init_func=init, interval=20, blit=False)
plt.savefig("animation_propagation")
plt.legend()
plt.show()
#####################################################################
# the distribution of intensity in the transverse plane

w0 = 40                                 
z = 0 
r_pts = np.arange(-10, 10, 0.1)
x, y = np.meshgrid(r_pts, r_pts)
r = x**2 + y**2
I = i_propagation_dis(I0, w0, r, z, k0, lamb)

# create the first figure
fig = plt.figure(figsize=(15,15))
im = plt.imshow(I)
plt.savefig("decay of intensity at the transverse plan")
plt.legend()
plt.show()

#####################################################################
#                                                                   #
#                    poputation of state                            #
#                                                                   #
#####################################################################

# we regard the electric field as a gaussian because W can't change with E quickly

# ammosov method to find rate of ionization
def rate(Z, omega0, U_i, E_gau, N_state, Nt):
    for j in range(Nt): 
        for i in range(N_state):
            W[j,i] = ammosov(Z[i], omega0, U_i[i], E_gau[j])
    return W

# solve the ODE equation of a stepwise ionization process
def population(t_pts, W, N_state, Nt):
    for i in range(7):
        if i==0:
            for j in range(Nt):
                if j==0:                                    # the first point of time
                    t_temp =t_pts[0:j+2]
                    y_temp = ode_0(t_temp, W[j,0], y0)
                    N[0,0] = y_temp[0]
                elif j==Nt-1:                               # the last point of time
                    t_temp =t_pts[j-1:j+1]
                    y_temp = ode_0(t_temp, W[j,0], N[j-1,0])
                    N[j,0] = y_temp[1]
                else:                
                    t_temp =t_pts[j-1:j+2]
                    y_temp = ode_0(t_temp, W[j,0], N[j-1,0])
                    N[j,0] = y_temp[1]
        else:
            for j in range(Nt):
                if j==0:
                    t_temp =t_pts[0:j+2]

                    y_temp = ode_j(t_temp, N[j,i-1], W[j,i], W[j,i-1], yj)

                    N[0,i] = y_temp[0]
                elif j==Nt-1:
                    t_temp =t_pts[j-1:j+1]

                    y_temp = ode_j(t_temp, N[j,i-1], W[j,i], W[j,i-1], N[j-1,i])

                    N[j,i] = y_temp[1]
                
                else:                
                    t_temp =t_pts[j-1:j+2]

                    y_temp = ode_j(t_temp, N[j,i-1], W[j,i], W[j,i-1], N[j-1,i])

                    N[j,i] = y_temp[1]
    return N

# degree of ionization = electron density 
def degree(N, t_pts):
    """The degree of ionization, return an array with length M
    
    Parameters:
    ----------
    N : population of the charge state j according to time (length=M*N)
    t_pts : a grid of time (length=M)
    
    Return:
    ------
    sum : the total number of released electron according to time
    """
    sum = np.zeros(len(t_pts))
    
    # calcul degree of ionization
    for i in range(len(t_pts)):
        for j in range(7):
            sum[i] += j*N[i,j]
    return sum

# dZ/dt the temporal derivative of the degree of ionization 
def dzdt(deg, t_pts):
    """The temporal derivative of the degree of ionization, return an array with length M-1, valable when dt is enough tiny
    
    Parameters:
    ----------
    deg : degree of ionization according to time (length=M)
    t_pts : a grid of time with unit fs (length=M)

    Return:
    ------
    sum : the total number of released electron according to time (length=M-1)
    """

    dzdt = np.zeros(len(t_pts)-1)
    
    #calcul the derivative
    for i in range(len(t_pts)-1):
        dzdt[i] = (deg[i+1]-deg[i])/(t_pts[i+1]-t_pts[i])
    return dzdt
    
######################################################################
# Ar element, 100um length of interaction, 1E16 W/cm^2 intensity 
N_state = 7
lam = lamb
Nt = M

# just consider the first 6 ionization states
W = np.zeros([Nt, 7])
U_i = np.array((15.76, 27.63, 40.74, 59.81, 75.02, 91.01, 124.32))
Z = np.arange(1, 8, 1)
N = np.zeros([Nt, 7])
N[0, 0] = 1                                # initial population 
p = np.zeros(Nt)                  
y0=1
yj=0

# calculer the field 
#E_gau = np.abs(ini_gau)
E_gau = gaussian(E0, t_pts, t_FWHM, t0)

# calculer the rate of ionization and population of state
W = rate(Z, omega0, U_i, E_gau, N_state, Nt)
N = population(t_pts, W, N_state, Nt)  

# degree of the ionization and dz/dt
deg_z = degree(N, t_pts)
der_dzdt = dzdt(deg_z, t_pts_fs)

t_pts_fs_der = t_pts_fs[:-1]            

# plot the four figures
fig = plt.figure(figsize=(21, 21))

ax1 = fig.add_subplot(2,2,1)     
ax1.plot(t_pts_fs, E_gau,lw=3)
ax1.set_title('Envelope of electric field', fontsize=35, fontname ='Times New Roman')
ax1.set_ylabel('Electric field E(V/m)', fontsize=28, fontname ='Times New Roman')
ax1.set_xlabel('Time (fs)', fontsize=28, fontname ='Times New Roman')
ax1.set_xlim(0,400)

ax2 = fig.add_subplot(2,2,2)     
ax2.plot(t_pts_fs, W[:,0], label='0' ,lw=2)
ax2.plot(t_pts_fs, W[:,1], label='+1',lw=2)
ax2.plot(t_pts_fs, W[:,2], label='+2',lw=2)
ax2.plot(t_pts_fs, W[:,3], label='+3',lw=2)
ax2.plot(t_pts_fs, W[:,4], label='+4',lw=2)
ax2.plot(t_pts_fs, W[:,5], label='+5',lw=2)
ax2.plot(t_pts_fs, W[:,6], label='+6',lw=2)
ax2.set_title('Rate of ionization', fontsize=35, fontname ='Times New Roman')
ax2.set_ylabel('Rate of ionization W', fontsize=30, fontname ='Times New Roman')
ax2.set_xlabel('Time (fs)', fontsize=30, fontname ='Times New Roman')
ax2.set_xlim(0,400)

ax3 = fig.add_subplot(2,2,3)    
ax3.semilogy(t_pts_fs, N[:,0], label='0',lw=3)
ax3.semilogy(t_pts_fs, N[:,1], label='+1',lw=3)
ax3.semilogy(t_pts_fs, N[:,2], label='+2',lw=3)
ax3.semilogy(t_pts_fs, N[:,3], label='+3',lw=3)
ax3.semilogy(t_pts_fs, N[:,4], label='+4',lw=3)
ax3.semilogy(t_pts_fs, N[:,5], label='+5',lw=3)
ax3.semilogy(t_pts_fs, N[:,6], label='+6',lw=3)
ax3.set_title('Population of the state', fontsize=35, fontname ='Times New Roman')
ax3.set_ylabel('Population', fontsize=30, fontname ='Times New Roman')
ax3.set_xlabel('Time (fs)', fontsize=30, fontname ='Times New Roman')
ax3.set_ylim(1E-6,2)
ax3.set_xlim(0,400)

ax4 = fig.add_subplot(2,2,4)    
ax4.plot(t_pts_fs,deg_z,lw=3)
ax4.set_xlim(0,400)
ax4.set_ylabel('Degree of ionization', fontsize=30, fontname ='Times New Roman')
ax4.set_xlabel('Time (fs)', fontsize=30, fontname ='Times New Roman')

ax5 = ax4.twinx()
ax5.semilogy(t_pts_fs_der, der_dzdt, lw=3)
ax5.set_ylabel('dZ/dt($fs^{-1}$)' , fontsize=30, fontname ='Times New Roman')
ax5.set_title('Degree of ionization and dZ/dt' , fontsize=35, fontname ='Times New Roman')
ax5.set_ylim(1E-4,5E-1)
ax5.set_xlim(0,400)

plt.legend('best')

plt.savefig('Degree of ionization')

plt.show()
####################################################################
# dzdt and degree of ionization z

fig = plt.figure(figsize=(15,9))
ax1 = plt.subplot()
ax2 = ax1.twinx()
ax1.set_title('Degree of ionization and dZ/dt', fontsize=35, fontname ='Times New Roman')   
ax1.semilogy(t_pts_fs_der, der_dzdt,'r', lw=3)
ax1.set_ylabel('dZ/dt($fs^{-1}$)', fontsize=28, fontname ='Times New Roman')
ax1.set_xlabel('Time(fs)', fontsize=28, fontname ='Times New Roman')
ax1.set_xlim(0,400)
ax1.set_ylim(1E-4,5E-1)  

ax2.plot(t_pts_fs,deg_z,lw=3)
ax2.set_ylabel('Degree of ionization', fontsize=28, fontname ='Times New Roman')
plt.savefig('dZdt')
plt.show()




#####################################################################
#                                                                   #
#                  propagation in plasma                            #
#                                                                   #
#####################################################################

# density of electron has a linear relation with dz/dt 
dens_electron = 3E8*Ni * deg_z

#####################################################################
#the electron density in plasma
fig =plt.figure(figsize=(15, 10))

ax= plt.subplot()
ax.plot(t_pts_fs, dens_electron,'g', lw=3, label='n(t)')
ax.set_xlabel('Time t (fs)', fontsize=28, fontname ='Times New Roman')
ax.set_ylabel('Electron density n(t) ($m^{-3}$)',  fontsize=28, fontname ='Times New Roman')
ax.set_title('Electron density n(t)', fontsize=35, fontname ='Times New Roman')
ax.set_xlim(0,400)
plt.savefig('current_in_plasma')
plt.legend()
plt.show()

#####################################################################
# the initial condition
n = 1                                      # don't consider fractive index 
v = c/n
L = 100E-6                                 # the length of interaction in one dimension  

N = 1000                                  
x_pts, dx = np.linspace(0, L, N, retstep=True)

dt = dx/c                                  # make sure that dt is about 1fs
M = 3000                                   # choose this number to make sure that a pulse pass through the length 
T = (M-1)*dt                               # the considering time 
t_pts = np.linspace(0, T, M)


# the gird position and time with the unit um and fs
t_pts_fs, dt_fs = np.linspace(0, T*1E15, M, retstep=True)
x_pts_um, dx_um = np.linspace(0, L*1E6, N, retstep=True)

# the  initial pulse and distribution
t_FWHM = 100E-15
x_FWHM = v*t_FWHM
t0 = 200E-15
x0 = -v*t0

ini_gau = gaussian_wavepacket(E0, omega0, t_FWHM, t_pts, t0)
ini_dis = distribution(E0, k0, x_FWHM, x_pts, x0)

#####################################################################

# instantiation of the class properties  
pro_plasma = inter(ini_gau, ini_dis,0 ,0 , L, T, N, M)

# make laser propagate 
pro_plasma.propagation(dens_electron)

# the grid point of frequency 
freq_pts = np.fft.fftfreq(M, dt)

print('dx = ', dx)
               
print('dt = ', dt*1E15)

print('time = ', T*1E15)

print('length = ', L)

print('dfreq2 = ', freq_pts[1]- freq_pts[0])
####################################################################
#the current in the center of plasma
fig =plt.figure(figsize=(15, 9))

ax= plt.subplot()
ax.plot(t_pts_fs, pro_plasma.j[:,0],'g',lw=2)
ax.set_xlabel('Time t (fs)', fontsize=28, fontname ='Times New Roman')
ax.set_ylabel('Current',  fontsize=28, fontname ='Times New Roman')
ax.set_title('Current in plasma', fontsize=35, fontname ='Times New Roman')
ax.set_xlim(0,400)
plt.savefig('current_in_plasma')
plt.legend()
plt.show()



####################################################################
# the frequency spectrum
freq_plasma = frequency(pro_plasma.e_right[:,999])
freq_plasma_abs = np.abs(freq_plasma)


# plot the spectrum of frequency 
fig, axes = plt.subplots(ncols=3, figsize=(20, 11))
ax = axes[0]
ax = one_scale(ax, t_pts_fs, pro_plasma.e_right[:,999],'royalblue', '-', 'Temporal pulse', 'E(t)', 'Time (fs)', 'Electric filed(V/m)')
ax.set_xlim(333,733)

ax = axes[1]
ax = one_scale(ax, freq_pts, freq_plasma_abs, 'g', '-', 'Frequency spectrum', 'P(f)', 'Frequency f (Hz)', 'Itensity')
ax.set_xlim(-0.55E15,0.55E15)

ax = axes[2]
ax = one_scale(ax, freq_pts, freq_plasma_abs, 'g', '-', 'Frequency spectrum(positive)', 'P(f)', 'Frequency f (Hz)', 'Intensity')
ax.set_xlim(0.40E15,0.60E15)

plt.savefig("spectrum_plasma")
plt.legend()
plt.show()

#################################################################

fig = plt.figure(figsize=(15, 9))

ax3 = plt.subplot()
ax3 = one_scale(ax3, 2*pi*freq_pts, freq_plasma_abs, 'g', '-', 'Frequency spectrum', 'line', 'Frequency spectrum $\omega$(rad/s)', 'Intensity')
ax3.set_xlim(2.5E15,3.5E15)
plt.savefig("frequency_spectrum_plasma")
plt.legend()
plt.show()


######################################################################
# calculer the average dzdt
sumdzdt = 0
t_sum = 0

for i in range(M-1):
    if der_dzdt[i]>=1E-3:
        sumdzdt += der_dzdt[i]
        t_sum += 1

der_dzdt_average = sumdzdt/t_sum
 
   
#####################################################################
#the lambda in plasma
lamb_pts = np.zeros(1)
freq_plasma_pos = np.zeros(1)

lamb_pts = fre_to_lam_pts(freq_pts, freq_plasma_abs)
freq_plasma_pos = fre_to_lam(freq_pts, freq_plasma_abs)


lamb_pts_nm =lamb_pts*1E9

      
np.delete(lamb_pts, [0])
np.delete(freq_plasma_pos, [0])


#the lambda  in vacuum
dis_freq = dis_freq_vacuum
lamb_pts1 = np.zeros(1)
freq_pos1 = np.zeros(1)
lamb_pts1 = fre_to_lam_pts(freq_pts, dis_freq)
freq_pos1 = fre_to_lam(freq_pts, dis_freq)
lamb_pts_nm1 =lamb_pts1*1E9
np.delete(lamb_pts1 , [0])
np.delete(freq_pos1, [0])


# the average shift
print(der_dzdt_average)
d_lam1 = delta_lamda_deg(L, lamb, Ni, der_dzdt_average)
print(d_lam1)

new_lamb_pts1 = d_lam1 +lamb_pts1
new_lamb_pts_nm1 = new_lamb_pts1*1E9
print(new_lamb_pts1)
print(len(new_lamb_pts1))

fig = plt.figure(figsize=(15, 9))
ax= plt.subplot()
ax.plot(lamb_pts_nm1, freq_pos1 , 'b', label = 'in vacuum')
ax.plot(lamb_pts_nm, freq_plasma_pos, 'g', label = 'shift in plasma')
#ax.plot(new_lamb_pts_nm1, freq_pos1, 'r', label = 'shift in palsma(by dZ/dt) ')
ax.set_xlabel('wavelength $\lambda(nm)$', fontsize=28, fontname ='Times New Roman')
ax.set_ylabel('Intensity',  fontsize=28, fontname ='Times New Roman')
ax.set_title('Wavelength in plasma', fontsize=35, fontname ='Times New Roman')
ax.set_xlim(500,640)
plt.savefig('wavelength_in_plasma')
plt.legend()
plt.show()

######################################################################
