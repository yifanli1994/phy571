import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation

import data1 as da1
import data2 as da2
import data3 as da3
import data4 as da4
import data5 as da5
import matplot as mat    

# Physcial parameters 
pi = np.pi
c = 3E8                                    # the velocity of light (m*s^-1)
e = -1.60E-19                              # charge of electron (C)
m = 9.11E-31                               # mass of electron (kg)
epsilon0 = 8.85E-12                        # vacuum permittivity (F*m^−1) 
Ni = 1.34E26                               # densiy of ion (m^-3)
lamb = 620E-9                              # wavelength (m) 
f = c/lamb                                 # frequency
I0 = 1E20                                  # intensity (W/m^2)
E0  = ((1E20)*377)**0.5                    # the amplitude (V/m)
omega0 =(2*pi*c)/lamb                      # the angular frequency
t_FWHM = 100E-15                           # half of the W (s) = 100fs
t0= 200E-15                                # the center of the gaussian wacepacket (s)
n0 = 1                                     # fractive index 
v = c/n0
k0 = omega0/v

######################################################################

dis_plasma_pos1 = da1.dis_plasma_pos
dis_plasma_pos2 = da2.dis_plasma_pos
dis_plasma_pos3 = da3.dis_plasma_pos
dis_plasma_pos4 = da4.dis_plasma_pos
dis_plasma_pos5 = da5.dis_plasma_pos
lamb_plasma_pts_nm1 = da1.lamb_plasma_pts_nm
lamb_plasma_pts_nm2 = da2.lamb_plasma_pts_nm
lamb_plasma_pts_nm3 = da3.lamb_plasma_pts_nm
lamb_plasma_pts_nm4 = da4.lamb_plasma_pts_nm
lamb_plasma_pts_nm5 = da5.lamb_plasma_pts_nm


######################################################################

fig = plt.figure(figsize=(15, 19))
ax= plt.subplot()

ax.plot(lamb_plasma_pts_nm1, dis_plasma_pos1+6E13, label ='1atm')
ax.plot(lamb_plasma_pts_nm2, dis_plasma_pos2+4.5E13, label ='2atm')
ax.plot(lamb_plasma_pts_nm3, dis_plasma_pos3+3E13, label ='3atm')
ax.plot(lamb_plasma_pts_nm4, dis_plasma_pos4+1.5E13, label ='4atm')
ax.plot(lamb_plasma_pts_nm5, dis_plasma_pos5, label ='5atm')
#ax.plot(new_lamb_pts_nm, dis_vacuum_pos, 'r', label = 'shift in palsma(by dZ/dt) ')
ax.set_xlabel('wavelength $\lambda(nm)$', fontsize=28, fontname ='Times New Roman')
ax.set_ylabel('Intensity',  fontsize=28, fontname ='Times New Roman')
ax.set_title('Wavelength in plasma', fontsize=35, fontname ='Times New Roman')
ax.set_xlim(500,640)
ax.set_yticks([])
plt.legend()
plt.savefig('wavelength_in_plasma')
plt.show()

######################################################################
