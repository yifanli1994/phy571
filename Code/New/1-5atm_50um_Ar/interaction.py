import numpy as np

# Physcial parameters 
pi = np.pi
c = 3E8                                    # the velocity of light (m*s^-1)
e = -1.60E-19                              # charge of electron (C)
m = 9.11E-31                               # mass of electron (kg)
epsilon0 = 8.85E-12                        # vacuum permittivity (F*m^−1) 
Ni = 1.34E26                               # densiy of ion (m^-3)
lamb = 620E-9                              # wavelength (m) 
f = c/lamb                                 # frequency
I0 = 1E20                                  # intensity (W/m^2)
E0  = ((1E20)*377)**0.5                    # the amplitude (V/m)
omega0 =(2*pi*c)/lamb                      # the angular frequency
t_FWHM = 100E-15                           # half of the W (s) = 100fs
t0= 200E-15                                # the center of the gaussian wacepacket (s)
n0 = 1                                     # fractive index 
v = c/n0
k0 = omega0/v

#####################################################################
#                                                                   #
#                  class including properties                       #
#                                                                   #
#####################################################################

# a class including properties in the interaction
class inter():
    """ This class has the whole properties of the interaction of laser and the medium.
        'field_e', 'current', 'index_f', 'density_e', 'deg_ion', 'der_degree'
    """
    
    def __init__(self, ini_gau, ini_dis, ini_gau_left, ini_dis_left, L, T, N, M):
        """
        Parameters:
        ----------
        int_gau : the initial guassian temporal pulse of right light
        ini_dis : the initial guassian distribution of right light
        ini_gau_left : the initial guassian temporal pulse of left light
        ini_dis_left : the initial guassian distribution of left light
        L : the length of interaction 
        T : the time of propagation
        N : the number of the grid position 
        M : the number of the grid time
        """
        self.ini_gau = ini_gau                   # initial temporal pulse of right light 
        self.ini_dis = ini_dis                   # initial distribution of pulse of right light
        self.ini_gau_left = ini_gau_left         # initial temporal pulse of left light
        self.ini_dis_left = ini_dis_left         # initial distribution of pulse of left light
        self.L = L 
        self.T = T
        self.N = N 
        self.M = M 
        
        self.t_pts, self.dt = np.linspace(0, T, M ,retstep=True)
        self.x_pts, self.dx = np.linspace(0, L, N, retstep=True)

        # initialize these proerties 
        self.e_right = np.zeros((M, N))           # the electric field propagate in the right
        self.e_left = np.zeros((M, N))            # the electric field propagate in the left
        self.j = np.zeros((M, N))                 # the current j
        self.n = np.ones((M, N))                  # the refraction index n
        self.dens_e = np.zeros((M, N))            # the density of electrons
        self.deg_ion = np.zeros((M, N))           # the density of electrons
        self.e = self.e_right + self.e_left       # the total electric field
        
        # initial condition
        self.e_right[:, 0] = self.ini_gau
        self.e_right[0, :] = self.ini_dis
        self.e_left[:, N-1] = self.ini_gau_left
        self.e_left[0, :] = self.ini_dis_left
        self.j[0, :] = 0 
        
    def propagation(self, dens_e):
        """This function updates the data of matrix according to time and position
        """
        self.dens_e[:, 0] = dens_e
        
        for j in range(1, self.N):
            for i in range(self.M-1, 1, -1):
                self.dens_e[i, j] = self.dens_e[i-1, j-1]
                
        for i in range(self.M-1):
            
            # update transverse current j, dt is enough tiny, so we can consider that j(t+dt/2)=j(t)
            # update j(t+dt/2) using the electric field E(t)
            self.j[i+1, :] = self.j[i, :] + self.dt*((e**2)/m)*self.dens_e[i, :]*(self.e_right[i, :]+self.e_right[i, :])
    
            # update electric field in two directions
            for j in range(0, self.N-1):
                self.e_right[i+1, j+1] = self.e_right[i, j] - pi*self.dt*(self.j[i+1, j] + self.j[i+1,j+1])
    
            for j in range(self.N-2, 0, -1):  
                self.e_left[i+1, j] = self.e_left[i, j+1] - pi*self.dt*(self.j[i+1, j] + self.j[i+1,j+1])
        # update 
        self.e = self.e_right + self.e_left
 
#####################################################################
#                                                                   #
#                     shift of wavelength                           #
#                                                                   #
#####################################################################

# the delta lambda as a function of density of electron
def delta_lamda_dens(L, lam, der_dens):
    """The delta lambda according to the Drude model and using the temporal derivative of electron densiy 
    Parameters:
    ----------
    L : interaction length 
    lam : wavelength lambda
    der_dens : derivative of electron density
    
    Return:
    ------
    delta_lam : the shift of the wavelength
    """
    
    delta_lam = -((e**2 * L * lam**3)/(2*pi*m*c**3))*der_dens
    return delta_lam

# the delta lambda as a function of dz/dt
def delta_lamda_deg(L, lam, Ni, der_deg):
    """The delta lambda according to the Drude model and using the temporal derivative of the degree of ionization 
    
    Parameters:
    ----------
    L : interaction length 
    lam : wavelength lambda
    Ni : ion density
    der_deg : derivative of the degree of ionization
    
    Return:
    ------
    delta_lam : the shift of the wavelength
    """
    
    delta_lam = -(((e**2)*L*(lam**3)*Ni)/(8*pi*m*epsilon0*(c**3)))*der_deg

    return delta_lam

# the factor of delta lambda as a function of lambda^3
def factor_delta_lamda_deg(L, lam, Ni, der_deg):
    """The delta lambda according to the Drude model and using the temporal derivative of the degree of ionization 
    
    Parameters:
    ----------
    L : interaction length 
    lam : wavelength lambda
    Ni : ion density
    der_deg : derivative of the degree of ionization
    
    Return:
    ------
    delta_lam : the shift of the wavelength
    """
    
    delta_lam = -(((e**2)*L*(lam**3)*Ni)/(8*pi*m*epsilon0*(c**3)))

    return delta_lam

def shift_lam(lam_pts, dis, Ni, der, L):
    """The blueshift of wavelength under the condition of ionization, return an array(length=2*N)
    
    Parameters:
    ----------
    lam_pts : an array of wavelength
    dis_fre : distribution of wavelength corresponding to the grid wavelength
    Ni : ion density related with gas pressure
    der : derivative of the degree of ionization
    L : length of interaction 
    
    Return:
    ------
    couple : a coupled array of corresponding wavelength and distribution
    """
    N = len(lam_pts)

    
    delta_lam = delta_lamda_deg(L, lam_pts, Ni, der)
    
    lam_new = lam_pts + delta_lam
    
    couple = np.zeros((2, N))
    
    couple[0, :] = lam_new
    couple[1, :] = dis

    return couple

# the cross section provided by collisional ionization
def cross_section(i, E_e, N, P, q):
    """The cross section for collisional ionization from charge state i to charge state i+1
    
    Parameters:
    ----------
    E_e : electron energy
    N : number of the shell of electrons
    P : the ionization potential for the outer outer shell
    q : the number of electrons in each shell 
    
    Return:
    ------
    cross : cross section for collisional ionization
    """
    
    a = 9E-14     # tacke into account any effects caused by collisional ionization of excited state species
    b = c = 0
    sigma = 0 
    
    # the cross section fron i to i+1
    for j in range(N):
        sigma += (a*q[i][j])/(E_e*P[i][j]) * np.log(E_e/P[i][j]) * (1-b*np.exp(-c*(E_e/P[i][j]-1)))
    
    cross = sigma
    return cross

# the confocal parameter of a plasma
def confocal_parameter(f, D):
    
    # the confocal parameter 
    z_c = 8*lam*f**2/(pi*D**2)
    return z_c

# the adiabaticity parameter 
def adiabaticity(U_i, E0, omega0):
    """Adiabaticity parameter 
    
    Parameters : 
    ----------
    U_i : the ionization potential 
    E0 : amplitude of electric field
    omega0 : the frequency of laser
    
    Return :
    -------
    adi : the adiabaticity parameter  
    """
    #quiver energy
    U_q = ((e**2)*(E0**2))/(4*m*(omega0**2))
    
    adi = (U_i/(2*U_q))**0.5

    return adi

def cross_section(i, E_e, N, P, q):
    """The cross section for collisional ionization from charge state i to charge state i+1
    
    Parameters:
    ----------
    E_e : electron energy
    N : number of the shell of electrons
    P : the ionization potential for the outer outer shell
    q : the number of electrons in each shell 
    
    Return:
    ------
    cross : cross section for collisional ionization
    """
    
    a = 9E-14     # tacke into account any effects caused by collisional ionization of excited state species
    b = c = 0
    sigma = 0 
    
    # the cross section fron i to i+1
    for j in range(N):
        sigma += (a*q[i][j])/(E_e*P[i][j]) * np.log(E_e/P[i][j]) * (1-b*np.exp(-c*(E_e/P[i][j]-1)))
    
    cross = sigma
    return cross

def correction_average(f, D):
    
    # the confocal parameter 
    z_c = 8*lam*f**2/(pi*D**2)
    return z_c
