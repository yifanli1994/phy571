import numpy as np
import ammosov as am
import ode as ode

# Physcial parameters 
pi = np.pi
c = 3E8                                    # the velocity of light (m*s^-1)
e = -1.60E-19                              # charge of electron (C)
m = 9.11E-31                               # mass of electron (kg)
epsilon0 = 8.85E-12                        # vacuum permittivity (F*m^−1) 
Ni = 1.34E26                               # densiy of ion (m^-3)
lamb = 620E-9                              # wavelength (m) 
f = c/lamb                                 # frequency
I0 = 1E20                                  # intensity (W/m^2)
E0  = ((1E20)*377)**0.5                    # the amplitude (V/m)
omega0 =(2*pi*c)/lamb                      # the angular frequency
t_FWHM = 100E-15                           # half of the W (s) = 100fs
t0= 200E-15                                # the center of the gaussian wacepacket (s)
n0 = 1                                     # fractive index 
v = c/n0
k0 = omega0/v

#####################################################################
#                                                                   #
#                    poputation of state                            #
#                                                                   #
#####################################################################

# we regard the electric field as a gaussian because W can't change with E quickly

# initial condition for population
y0=1
yj=0

# ammosov method to find rate of ionization
def rate(Z, omega0, U_i, E_gau, N_state, Nt):
    W = np.zeros([Nt, N_state])
    
    for j in range(Nt): 
        for i in range(N_state):
            W[j,i] = am.ammosov(Z[i], omega0, U_i[i], E_gau[j])
    return W

# solve the ODE equation of a stepwise ionization process
def population(t_pts, W, N_state, Nt):
    N = np.zeros([Nt, N_state])
    N[0, 0] = 1                                # initial population 

    for i in range(7):
        if i==0:
            for j in range(Nt):
                if j==0:                                    # the first point of time
                    t_temp =t_pts[0:j+2]
                    y_temp = ode.ode_0(t_temp, W[j,0], y0)
                    N[0,0] = y_temp[0]
                elif j==Nt-1:                               # the last point of time
                    t_temp =t_pts[j-1:j+1]
                    y_temp = ode.ode_0(t_temp, W[j,0], N[j-1,0])
                    N[j,0] = y_temp[1]
                else:                
                    t_temp =t_pts[j-1:j+2]
                    y_temp = ode.ode_0(t_temp, W[j,0], N[j-1,0])
                    N[j,0] = y_temp[1]
        else:
            for j in range(Nt):
                if j==0:
                    t_temp =t_pts[0:j+2]

                    y_temp = ode.ode_j(t_temp, N[j,i-1], W[j,i], W[j,i-1], yj)

                    N[0,i] = y_temp[0]
                elif j==Nt-1:
                    t_temp =t_pts[j-1:j+1]

                    y_temp = ode.ode_j(t_temp, N[j,i-1], W[j,i], W[j,i-1], N[j-1,i])

                    N[j,i] = y_temp[1]
                
                else:                
                    t_temp =t_pts[j-1:j+2]

                    y_temp = ode.ode_j(t_temp, N[j,i-1], W[j,i], W[j,i-1], N[j-1,i])

                    N[j,i] = y_temp[1]
    return N

# degree of ionization = electron density 
def degree(N, t_pts):
    """The degree of ionization, return an array with length M
    
    Parameters:
    ----------
    N : population of the charge state j according to time (length=M*N)
    t_pts : a grid of time (length=M)
    
    Return:
    ------
    sum : the total number of released electron according to time
    """
    sum = np.zeros(len(t_pts))
    
    # calcul degree of ionization
    for i in range(len(t_pts)):
        for j in range(7):
            sum[i] += j*N[i,j]
    return sum

# dZ/dt the temporal derivative of the degree of ionization 
def dzdt(deg, t_pts):
    
    """The temporal derivative of the degree of ionization, return an array with length M-1, valable when dt is enough tiny
    
    Parameters:
    ----------
    deg : degree of ionization according to time (length=M)
    t_pts : a grid of time with unit fs (length=M)

    Return:
    ------
    sum : the total number of released electron according to time (length=M-1)
    """

    dzdt = np.zeros(len(t_pts)-1)
    
    #calcul the derivative
    for i in range(len(t_pts)-1):
        dzdt[i] = (deg[i+1]-deg[i])/(t_pts[i+1]-t_pts[i])
    return dzdt


