import numpy as np

# Physcial parameters 
pi = np.pi
c = 3E8                                    # the velocity of light (m*s^-1)
e = -1.60E-19                              # charge of electron (C)
m = 9.11E-31                               # mass of electron (kg)
epsilon0 = 8.85E-12                        # vacuum permittivity (F*m^−1) 
Ni = 1.34E26                               # densiy of ion (m^-3)
lamb = 620E-9                              # wavelength (m) 
f = c/lamb                                 # frequency
I0 = 1E20                                  # intensity (W/m^2)
E0  = ((1E20)*377)**0.5                    # the amplitude (V/m)
omega0 =(2*pi*c)/lamb                      # the angular frequency
t_FWHM = 100E-15                           # half of the W (s) = 100fs
t0= 200E-15                                # the center of the gaussian wacepacket (s)
n0 = 1                                     # fractive index 
v = c/n0
k0 = omega0/v

#####################################################################
#                                                                   #
#                 Ammosov et al. field-ionization                   #
#                                                                   #
#####################################################################

# the Ammosov et al. field-ionization
def ammosov(Z, omega0, U_i, E):
    """Ammosov et al. formula
    This formula is in better agreement with data.
    
    Parameters : 
    ----------
        Z : the residual charge seen by this electron
        U_i : the ionization  
        E : electric field
     
    Return :
    -------
    W : fidld-ionization .   
    """
    
    omega_au = 4.1E16                         # Atomic unit of frequency (s^-1)   
    E_au = 5.14E11                            # Atomic unit of electric field  (V*m^-1)    
    U_H = 13.6                                # The ionization potential of Hydrogen (eV)    
    n_eff = Z/((U_i/U_H)**0.5)                # The effective quantum number 
    # The ionization rate
    if E!=0:
        k_E = E_au/E
        W = 1.61*omega_au*((Z**2/n_eff**4.5) * ((10.87 * (Z**3/n_eff**4) * (k_E))**(2*n_eff-1.5)))*np.exp(-(2/3.) * ((Z**3)/(n_eff**3)) * (k_E))
    else:
        W = 0
    return W

def ammosov2(Z, omega0, U_i, E, l, m):
    """Ammosov et al. formula provided by second paper
    
    Parameters : 
    ----------
        Z : the residual charge seen by this electron
        U_i : the ionization  
        E : electric field
     
    Return :
    -------
    W : fidld-ionization .   
    """
    
    omega_au = 4.1E16                        # Atomic unit of frequency (s^-1)   
    E_au = 5.14E11                           # Atomic unit of electric field  (V*m^-1)   
    U_H = 13.6                               # The ionization potential of Hydrogen (eV)   
    n_eff = Z/((U_i/U_H)**0.5)               # The effective quantum number     
    eu = 2.7128                              # Euler's number                          

    C_n = ((2*eu/n_eff)**n_eff)*((2*pi*n_eff)**(-0.5))
    
    term1 = ((2*l+1) * np.math.factorial(l+np.abs(m)))/((2**(np.abs(m))) * (np.math.factorial(np.abs(m))) *(np.math.factorial(l-np.abs(m))))
    
    term2 = (2 * ((U_i/U_H)**(3/2)) * (E_au/E))**(2*n_eff-np.abs(m)-1)
    
    term3 = np.exp((-2/3) * ((U_i/U_H)**(3/2)) * (E_au/E))
    
    W_st = 0.5*omega_au*(C_n**2)*(U_i/U_H)*term1*term2*term3
    
    # The mean ionization rate in a sinusoidal field
    W = ((3/pi) * ((U_i/U_H)**(3/2)) * (E_au/E))**0.5 * W_st
    
    return W


