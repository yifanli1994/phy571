import numpy as np

# Physcial parameters 
pi = np.pi
c = 3E8                                    # the velocity of light (m*s^-1)
e = -1.60E-19                              # charge of electron (C)
m = 9.11E-31                               # mass of electron (kg)
epsilon0 = 8.85E-12                        # vacuum permittivity (F*m^−1) 
Ni = 1.34E26                               # densiy of ion (m^-3)
lamb = 620E-9                              # wavelength (m) 
f = c/lamb                                 # frequency
I0 = 1E20                                  # intensity (W/m^2)
E0  = ((1E20)*377)**0.5                    # the amplitude (V/m)
omega0 =(2*pi*c)/lamb                      # the angular frequency
t_FWHM = 100E-15                           # half of the W (s) = 100fs
t0= 200E-15                                # the center of the gaussian wacepacket (s)
n0 = 1                                     # fractive index 
v = c/n0
k0 = omega0/v

#####################################################################
#                                                                   #
#                       Gaussian function                           #
#                                                                   #
#####################################################################

# Gaussian temporal pulse
def gaussian_wavepacket(E0, omega0, t_FWHM, t, t0):
    """The laser intensity is Gaussian temporal pulse
    
    Parameters:
    ----------
    E0 : Amplitude of electric field
    omega0 : Frequency of the source laser
    t_FWHM : full width at half maximun FWHM
    t0 : the center of the gaussian wavepacket
    
    Return:
    ------
    E : electric field of guassia wavepacket
    """
    
    delta_t = t_FWHM/(2*np.sqrt(np.log(2)))
    # The electric field
    E = E0 * np.cos(omega0*(t-t0)) * np.exp(-((t-t0)**2)/(2*delta_t**2))
    return E 

# Propagation of the pulse along with x
def distribution(E0, k0,  x_FWHM, x, x0):
    """The propagation of the pulse along the position x
    
    Parameters:
    ----------
    E0 : Amplitude of electric field
    omega0 : Frequency of the source laser
    x_FWHM : full width at half maximun FWHM
    t0 : the center of the gaussian wavepacket
    
    Return:
    ------
    E : electric field as a fonction of pposition
    """
    # velocity of the propagation and vector 

    delta_x = x_FWHM/(2*np.sqrt(np.log(2)))
    
    # the electric field
    E = E0 * np.cos(k0*(x-x0)) * np.exp(-((x-x0)**2)/(2*delta_x**2))
    return E

# Time average
def average_intensity(E, t, DT):
    """The time average intensity of the electric field 

    Parameters:
    ----------
    E: the electronic field
    t: the time
    DT: the resolution of the time average
    
    Return:
    ------
    I : time average intensiy
    """
    dt = t[1]-t[0]
    n_DT = int(0.5*DT/dt)

    # the average intensity
    temp = np.zeros(len(t))
    I = np.zeros(len(t))
    for i in range(len(t)):
        if i<=n_DT | i>=len(t)-n_DT:
            continue
        for j in range(n_DT):
            temp[i] += E[i+j]*E[i+j]
            temp[i] += E[i-j]*E[i-j]
        I[i] = (1./(2*dt*n_DT))*temp[i]
    return I

# Guassian
def gaussian(A0, x, x_FWHM, x0):
    """The gaussian function
    
    Parameters :
    ----------
    A0 : the amplitude 
    x : the x-axis
    x_FWHM : full width at half maximun FWHM
    x0 : the center
    """
    # variation
    delta_x = x_FWHM/(2*np.sqrt(np.log(2)))
    # gaussian function
    g = A0*np.exp(-((x-x0)**2)/(2*(delta_x**2)))
    return g

# Envelope of wave
def envelope(E):
    """
    Parameters:
    ----------
    E : the electric field
    
    Return :
    -------
    g : the envelope 
    """
    
    he = fftpack.hilbert(E)
    return np.sqrt(E**2 + he**2)

# Electric field propagation equation
def e_propagation_dis(E0, w0, r, z, k, lamb):

    z_r = pi*w0**2/lamb
    
    w_z = w0*np.sqrt(1+(z/z_r)**2)

    r_z = z*(1+(z_r/z)**2)

    E = E0*(w0/w_z)*np.exp(-r**2/(w_z**2))*cos(k*(z+r**2/(2*r_z**2)))
    
    return E

# Intensity propagation equation
def i_propagation_dis(I0, w0, r, z, k, lamb):
    
    z_r = pi*w0**2/lamb
    
    w_z = w0*np.sqrt(1+(z/z_r)**2)


    I = I0*(w0/w_z)**2*np.exp(-2*(r**2)/(w_z**2))
    
    return I