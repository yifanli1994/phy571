import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation

# Physcial parameters 
pi = np.pi
c = 3E8                                    # the velocity of light (m*s^-1)
e = -1.60E-19                              # charge of electron (C)
m = 9.11E-31                               # mass of electron (kg)
epsilon0 = 8.85E-12                        # vacuum permittivity (F*m^−1) 
Ni = 1.34E26                               # densiy of ion (m^-3)
lamb = 620E-9                              # wavelength (m) 
f = c/lamb                                 # frequency
I0 = 1E20                                  # intensity (W/m^2)
E0  = ((1E20)*377)**0.5                    # the amplitude (V/m)
omega0 =(2*pi*c)/lamb                      # the angular frequency
t_FWHM = 100E-15                           # half of the W (s) = 100fs
t0= 200E-15                                # the center of the gaussian wacepacket (s)
n0 = 1                                     # fractive index 
v = c/n0
k0 = omega0/v


#####################################################################
#                                                                   #
#                         Matplotlib                                #
#                                                                   #
#####################################################################

# two scale 
def two_scales(ax1, xaxis, data1, data2, c1, c2, title, lab1, lab2, xlabel, ylabel1, ylabel2):
    """

    Parameters:
    ----------
    ax : Axis to put two scales on
    xaxis : x-axis values for both datasets
    data1 : Data for left hand scale
    data2 : Data for right hand scale
    c1 : Color for line 1
    c2 : Color for line 2
    title : title of 
    lab1 :  label of 1 scale
    lab2 :  label of 2 scale
    xlabel :  xlabel 
    ylabel1 : ylabel of 1 scale 
    ylabel2 : ylabel of 2 scale 

    Returns:
    -------
    ax : Original axis
    ax2 : New twin axis
    """
    ax2 = ax1.twinx()
    
    ax1.set_title(title, fontsize=35, fontname ='Times New Roman')
    ax1.plot(xaxis, data1, lw=1, color=c1, label=lab1)
    ax1.set_xlabel(xlabel, fontsize=25, fontname ='Times New Roman')
    ax1.set_ylabel(ylabel1, fontsize=25, fontname ='Times New Roman')
    ax2.plot(xaxis, data2,'--', lw=1, color=c2, label=lab2)
    ax2.set_ylabel(ylabel2, fontsize=25, fontname ='Times New Roman')
    ax1.legend()
    ax2.legend()
    return ax1, ax2

# one scale
def one_scale(ax, xaxis, data, c, type_line, title, lab, xlabel, ylabel):
    """

    Parameters:
    ----------
    ax : Axis to put the scale on
    xaxis : x-axis values for the data
    data1 : Data for the scale
    c1 : Color for line 
    title :
    lab :  label of scale
    xlabel :  label of x axis
    ylabel :  label of y axis
    
    Returns:
    -------
    ax : Original axis
    """
    ax.plot(xaxis, data,type_line, lw=1, color=c, label=lab)
    ax.set_title(title, fontsize=35, fontname ='Times New Roman')
    ax.set_xlabel(xlabel, fontsize=25, fontname ='Times New Roman')
    ax.set_ylabel(ylabel, fontsize=25, fontname ='Times New Roman')
    ax.legend()
    return ax
