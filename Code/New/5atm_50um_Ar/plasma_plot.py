import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation

import data as da
import matplot as mat    

# Physcial parameters 
pi = np.pi
c = 3E8                                    # the velocity of light (m*s^-1)
e = -1.60E-19                              # charge of electron (C)
m = 9.11E-31                               # mass of electron (kg)
epsilon0 = 8.85E-12                        # vacuum permittivity (F*m^−1) 
Ni = 1.34E26                               # densiy of ion (m^-3)
lamb = 620E-9                              # wavelength (m) 
f = c/lamb                                 # frequency
I0 = 1E20                                  # intensity (W/m^2)
E0  = ((1E20)*377)**0.5                    # the amplitude (V/m)
omega0 =(2*pi*c)/lamb                      # the angular frequency
t_FWHM = 100E-15                           # half of the W (s) = 100fs
t0= 200E-15                                # the center of the gaussian wacepacket (s)
n0 = 1                                     # fractive index 
v = c/n0
k0 = omega0/v

#####################################################################
N = da.N
M =da.M
T = da.T
L = da.L
dt = da.dt
dx = da.dx
x_pts = da.x_pts
t_pts = da.t_pts
t_pts_fs = da.t_pts_fs
x_pts_um = da.x_pts_um
dens_electron = da.dens_electron
pro_plasma = da.pro_plasma
freq_pts = da.freq_pts
freq_plasma_abs = da.freq_plasma_abs

#####################################################################
#the electron density in plasma
fig =plt.figure(figsize=(15, 9))

ax= plt.subplot()
ax.plot(t_pts_fs, dens_electron,'g', lw=3, label='n(t)')
ax.set_xlabel('Time t (fs)', fontsize=28, fontname ='Times New Roman')
ax.set_ylabel('Electron density n(t) ($m^{-3}$)',  fontsize=28, fontname ='Times New Roman')
ax.set_title('Electron density n(t)', fontsize=35, fontname ='Times New Roman')
ax.set_xlim(0,400)
plt.savefig('current_in_plasma')
plt.legend()
plt.show()

####################################################################
#the current in the center of plasma
fig =plt.figure(figsize=(15, 9))

ax= plt.subplot()
ax.plot(t_pts_fs, pro_plasma.j[:,0],'g',lw=2)
ax.set_xlabel('Time t (fs)', fontsize=28, fontname ='Times New Roman')
ax.set_ylabel('Current',  fontsize=28, fontname ='Times New Roman')
ax.set_title('Current in plasma', fontsize=35, fontname ='Times New Roman')
ax.set_xlim(0,400)
plt.savefig('current_in_plasma')
plt.legend()
plt.show()



####################################################################

# plot the spectrum of frequency 
fig, axes = plt.subplots(ncols=3, figsize=(20, 11))
ax = axes[0]
ax = mat.one_scale(ax, t_pts_fs, pro_plasma.e[:,N-1],'royalblue', '-', 'Temporal pulse', 'E(t)', 'Time (fs)', 'Electric filed(V/m)')

ax = axes[1]
ax = mat.one_scale(ax, freq_pts, freq_plasma_abs, 'g', '-', 'Frequency spectrum', 'P(f)', 'Frequency f (Hz)', 'Itensity')
ax.set_xlim(-0.55E15,0.55E15)

ax = axes[2]
ax = mat.one_scale(ax, freq_pts, freq_plasma_abs, 'g', '-', 'Frequency spectrum(positive)', 'P(f)', 'Frequency f (Hz)', 'Intensity')
ax.set_xlim(0.40E15,0.60E15)

plt.savefig("spectrum_plasma")
plt.legend()
plt.show()

#################################################################

fig = plt.figure(figsize=(15, 9))

ax3 = plt.subplot()
ax3 = mat.one_scale(ax3, 2*pi*freq_pts, freq_plasma_abs, 'g', '-', 'Frequency spectrum', 'line', 'Frequency spectrum $\omega$(rad/s)', 'Intensity')
ax3.set_xlim(2.5E15,3.5E15)
plt.savefig("frequency_spectrum_plasma")
plt.legend()
plt.show()

