import numpy as np
import matplotlib.pyplot as plt
import data as da

import matplot as mat    

# Physcial parameters 
pi = np.pi
c = 3E8                                    # the velocity of light (m*s^-1)
e = -1.60E-19                              # charge of electron (C)
m = 9.11E-31                               # mass of electron (kg)
epsilon0 = 8.85E-12                        # vacuum permittivity (F*m^−1) 
Ni = 1.34E26                               # densiy of ion (m^-3)
lamb = 620E-9                              # wavelength (m) 
f = c/lamb                                 # frequency
I0 = 1E20                                  # intensity (W/m^2)
E0  = ((1E20)*377)**0.5                    # the amplitude (V/m)
omega0 =(2*pi*c)/lamb                      # the angular frequency
t_FWHM = 100E-15                           # half of the W (s) = 100fs
t0= 200E-15                                # the center of the gaussian wacepacket (s)
n0 = 1                                     # fractive index 
v = c/n0
k0 = omega0/v

####################################################################
N = da.N
M =da.M
T = da.T
L = da.L
dt = da.dt
dx = da.dx
x_pts = da.x_pts
t_pts = da.t_pts
t_pts_fs = da.t_pts_fs
x_pts_um = da.x_pts_um
t_pts_fs_der = da.t_pts_fs_der
E_gau = da.E_gau
W = da.W
N = da.N_pop
deg_z = da.deg_z
der_dzdt = da.der_dzdt
Nx = da.N
Mt = da.M
N_state = da.N_state

####################################################################
# plot the four figures
fig = plt.figure(figsize=(21, 21))

ax1 = fig.add_subplot(2,2,1)     
ax1.plot(t_pts_fs, E_gau ,lw=3)
ax1.set_title('Envelope of electric field', fontsize=35, fontname ='Times New Roman')
ax1.set_ylabel('Electric field E(V/m)', fontsize=28, fontname ='Times New Roman')
ax1.set_xlabel('Time (fs)', fontsize=28, fontname ='Times New Roman')
ax1.set_xlim(0,400)


ax2 = fig.add_subplot(2,2,2)     
ax2.plot(t_pts_fs, W[:,0], label='0' ,lw=2)
ax2.plot(t_pts_fs, W[:,1], label='+1',lw=2)
ax2.plot(t_pts_fs, W[:,2], label='+2',lw=2)
ax2.plot(t_pts_fs, W[:,3], label='+3',lw=2)
ax2.plot(t_pts_fs, W[:,4], label='+4',lw=2)
ax2.plot(t_pts_fs, W[:,5], label='+5',lw=2)
ax2.plot(t_pts_fs, W[:,6], label='+6',lw=2)
ax2.set_title('Rate of ionization', fontsize=35, fontname ='Times New Roman')
ax2.set_ylabel('Rate of ionization W', fontsize=30, fontname ='Times New Roman')
ax2.set_xlabel('Time (fs)', fontsize=30, fontname ='Times New Roman')
ax2.set_xlim(0,400)

ax3 = fig.add_subplot(2,2,3)    
ax3.semilogy(t_pts_fs, N[:,0], label='0',lw=3)
ax3.semilogy(t_pts_fs, N[:,1], label='+1',lw=3)
ax3.semilogy(t_pts_fs, N[:,2], label='+2',lw=3)
ax3.semilogy(t_pts_fs, N[:,3], label='+3',lw=3)
ax3.semilogy(t_pts_fs, N[:,4], label='+4',lw=3)
ax3.semilogy(t_pts_fs, N[:,5], label='+5',lw=3)
ax3.semilogy(t_pts_fs, N[:,6], label='+6',lw=3)
ax3.set_title('Population of the state', fontsize=35, fontname ='Times New Roman')
ax3.set_ylabel('Population', fontsize=30, fontname ='Times New Roman')
ax3.set_xlabel('Time (fs)', fontsize=30, fontname ='Times New Roman')
ax3.set_ylim(1E-6,2)
ax3.set_xlim(0,400)

ax4 = fig.add_subplot(2,2,4)    
ax4.plot(t_pts_fs, deg_z, lw=3)

ax4.set_ylabel('Degree of ionization', fontsize=30, fontname ='Times New Roman')
ax4.set_xlabel('Time (fs)', fontsize=30, fontname ='Times New Roman')
ax4.set_xlim(0,400)

ax5 = ax4.twinx()
ax5.semilogy(t_pts_fs_der, der_dzdt, lw=3)
ax5.set_ylabel('dZ/dt($fs^{-1}$)' , fontsize=30, fontname ='Times New Roman')
ax5.set_title('Degree of ionization and dZ/dt' , fontsize=35, fontname ='Times New Roman')
ax5.set_ylim(1E-4,5E-1)
ax5.set_xlim(0,400)
plt.legend('best')

plt.savefig('Degree of ionization')

plt.show()
####################################################################
# dzdt and degree of ionization z

fig = plt.figure(figsize=(15,9))
ax1 = plt.subplot()
ax2 = ax1.twinx()
ax1.set_title('Degree of ionization and dZ/dt', fontsize=35, fontname ='Times New Roman')   
ax1.semilogy(t_pts_fs_der, der_dzdt,'r', lw=3)
ax1.set_ylabel('dZ/dt($fs^{-1}$)', fontsize=28, fontname ='Times New Roman')
ax1.set_xlabel('Time(fs)', fontsize=28, fontname ='Times New Roman')
ax1.set_xlim(0,400)
ax1.set_ylim(1E-4,5E-1)  

ax2.plot(t_pts_fs,deg_z,lw=3)
ax2.set_ylabel('Degree of ionization', fontsize=28, fontname ='Times New Roman')
plt.savefig('dZdt')
plt.show()