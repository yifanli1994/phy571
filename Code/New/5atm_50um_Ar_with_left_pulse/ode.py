import numpy as np
from scipy.integrate import odeint
import pylab as pl
from scipy import fftpack

# Physcial parameters 
pi = np.pi
c = 3E8                                    # the velocity of light (m*s^-1)
e = -1.60E-19                              # charge of electron (C)
m = 9.11E-31                               # mass of electron (kg)
epsilon0 = 8.85E-12                        # vacuum permittivity (F*m^−1) 
Ni = 1.34E26                               # densiy of ion (m^-3)
lamb = 620E-9                              # wavelength (m) 
f = c/lamb                                 # frequency
I0 = 1E20                                  # intensity (W/m^2)
E0  = ((1E20)*377)**0.5                    # the amplitude (V/m)
omega0 =(2*pi*c)/lamb                      # the angular frequency
t_FWHM = 100E-15                           # half of the W (s) = 100fs
t0= 200E-15                                # the center of the gaussian wacepacket (s)
n0 = 1                                     # fractive index 
v = c/n0
k0 = omega0/v

#####################################################################
#                                                                   #
#                    ODE equation(scipy)                            #
#                                                                   #
#####################################################################

# solve the ODE equation dy/dt = -k*y
def ode_0(t, k, y0):
    """Solve the ODE with form dy/dt = -k*y
    
    Parameters:
    ---------
    t : variable
    k : the factor
    y0 : initial condition of the first variable
    
    Return :
    -------
    y : the solution under initial condition 
    """
    # function that returns dy/dt
    def model(y,t):
        dydt = -k*y
        return dydt
    y = odeint(model, y0, t)
    return y

# solve the ODE equation dy(t)/dt = -k*y(t) +h*g(t)   
def ode_j(t, g, k, h, y0):
    """Solve the ODE with form dy(t)/dt = -k*y(t) +h*g(t) 
    
    Parameters:
    ---------
    t : variable
    g : a term varing with variable
    k : factor of term y(t)
    h : factor of term g(t)
    y0 : initial condition of the first variable
    
    Return :
    -------
    y : the solution under initial condition 
    """
    # function that returns dy/dt
    def model(y,t):
        dydt = -k*y + h*g
        return dydt

    # solve ODE
    y = odeint(model, y0, t)
    return y