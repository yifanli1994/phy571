import numpy as np

# Physcial parameters 
pi = np.pi
c = 3E8                                    # the velocity of light (m*s^-1)
e = -1.60E-19                              # charge of electron (C)
m = 9.11E-31                               # mass of electron (kg)
epsilon0 = 8.85E-12                        # vacuum permittivity (F*m^−1) 
Ni = 1.34E26                               # densiy of ion (m^-3)
lamb = 620E-9                              # wavelength (m) 
f = c/lamb                                 # frequency
I0 = 1E20                                  # intensity (W/m^2)
E0  = ((1E20)*377)**0.5                    # the amplitude (V/m)
omega0 =(2*pi*c)/lamb                      # the angular frequency
t_FWHM = 100E-15                           # half of the W (s) = 100fs
t0= 200E-15                                # the center of the gaussian wacepacket (s)
n0 = 1                                     # fractive index 
v = c/n0
k0 = omega0/v


#####################################################################
#                                                                   #
#             spectrum and fourier transform                        #
#                                                                   #
#####################################################################
# Fourier transform and the spectrum of frequency 
def frequency(f):
    """
    Parameters:
    ----------
    f : the electric field
    
    Return :
    -------
    g : the spectrum of frequency 
    """
    g = np.fft.fft(f)
    return g

# change a frequency to wavelength from 100nm to 1000nm 
def fre_to_lam(freq_pts, dis):
    """Change the grid time to a grid frequency in a Fourier transform, then change into a grid of wavelength
    
    Parameters:
    ----------
    fre_pts :  frequency 3E14 3E15 
    
    Return :
    ------
    lam_pts : an array of wavelength 
    """
    lamb_pts = np.zeros(1)
    dis_lamb = np.zeros(1)
    
    for i in range(len(freq_pts)):
        if (freq_pts[i]>=3E14)&(freq_pts[i]<=3E15):
            lamb_pts = np.append(lamb_pts, v/freq_pts[i])
            dis_lamb = np.append(dis_lamb, dis[i])
        else: continue    
    np.delete(lamb_pts,[0])
    np.delete(dis_lamb,[0])
    
    return dis_lamb

# change frequency to wavelength from 100nm to 1000nm, return the distribution
def fre_to_lam_pts(freq_pts,dis):
    """Change the grid time to a grid frequency in a Fourier transform, then change into a grid of wavelength
    
    Parameters:
    ----------
    fre_pts : frequency 3E14 3E15 
    
    Return :
    ------
    lam_pts : an array of wavelength 
    """
    lamb_pts = np.zeros(1)
    dis_lamb = np.zeros(1)
    
    for i in range(len(freq_pts)):
        if (freq_pts[i]>=3E14)&(freq_pts[i]<=3E15):
            lamb_pts = np.append(lamb_pts, v/freq_pts[i])
            dis_lamb = np.append(dis_lamb, dis[i])
        else: continue

    np.delete(lamb_pts,[0])
    np.delete(dis_lamb,[0])
    
    return lamb_pts