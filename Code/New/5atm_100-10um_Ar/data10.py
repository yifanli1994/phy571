import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation
import pylab as pl
from scipy import fftpack
###################################################################
import ammosov as am                        # ammosov()             ammosov()
import interaction as inter                # class inter()         delta_lamda_dens() delta_lamda_deg
import spectrum as sp                      # frequency()           fre_to_lam()       fre_to_lam_pts
import ode as ode                          # ode_0()               ode_j()
import gaussian as ga                      # gaussian_wavepacket() distribution()     average_intensity()     gaussian() 
import state as st                         # rate()                population()       degree()                dzdt()
import matplot as mat    

# Physcial parameters 
pi = np.pi
c = 3E8                                    # the velocity of light (m*s^-1)
e = -1.60E-19                              # charge of electron (C)
m = 9.11E-31                               # mass of electron (kg)
epsilon0 = 8.85E-12                        # vacuum permittivity (F*m^−1) 
Ni = 1.34E26                               # densiy of ion (m^-3)
lamb = 620E-9                              # wavelength (m) 
f = c/lamb                                 # frequency
I0 = 1E20                                  # intensity (W/m^2)
E0  = ((1E20)*377)**0.5                    # the amplitude (V/m)
omega0 =(2*pi*c)/lamb                      # the angular frequency
t_FWHM = 100E-15                           # half of the W (s) = 100fs
t0= 200E-15                                # the center of the gaussian wacepacket (s)
n0 = 1                                     # fractive index 
v = c/n0
k0 = omega0/v

#####################################################################
#                                                                   #
#                  propagation in vacuum                            #
#                                                                   #
#####################################################################

# the initial condition
v = c/n0                                  # don't consider fractive index 
L = 10E-6                                 # the length of interaction in one dimension  

N = 100                                  
x_pts, dx = np.linspace(0, L, N, retstep=True)

dt = dx/c                                  # make sure that dt is about 1fs
M = 3000                                   # choose this number to make sure that a pulse pass through the length 
T = (M-1)*dt                               # the considering time 
t_pts = np.linspace(0, T, M)
dens_e0 = np.zeros(M)                      # electron density is zero in vacuum 

print('dx = ', dx)               
print('dt = ', dt*1E15)
print('time = ', T*1E15)
print('length = ', L*1E6)

# the gird position and time with the unit um and fs
t_pts_fs, dt_fs = np.linspace(0, T*1E15, M, retstep=True)
x_pts_um, dx_um = np.linspace(0, L*1E6, N, retstep=True)

# the  initial pulse and distribution
t_FWHM = 100E-15
x_FWHM = v*t_FWHM
t0 = 200E-15
x0 = -v*t0

ini_gau = ga.gaussian_wavepacket(E0, omega0, t_FWHM, t_pts, t0)
ini_dis = ga.distribution(E0, k0, x_FWHM, x_pts, x0)
ini_gau_l = ga.gaussian_wavepacket(E0, omega0, t_FWHM, t_pts, L/c+t0)
ini_dis_l = ga.distribution(E0, -k0, x_FWHM, x_pts, 2*L-x0)
#####################################################################
# instantiation of the class properties  
pro = inter.inter(ini_gau, ini_dis,0 ,0 , L, T, N, M)

# make laser propagate 
pro.propagation(dens_e0)

####################################################################
# frequency spectrum

# the grid point of frequency
Nt = M 
freq_pts = np.fft.fftfreq(Nt, dt)

# the frequency spectrum
E_t = ini_gau
dis_freq = sp.frequency(E_t)

freq_abs = np.abs(dis_freq)
freq_real = np.real(dis_freq)
freq_imag = np.imag(dis_freq)
dis_freq_vacuum = freq_abs
omega_pts = 2*pi*freq_pts

print('dfreq = ', freq_pts[1]- freq_pts[0])
####################################################################
####################################################################
# Ar element,  
N_state = 7
U_i = np.array((15.76, 27.63, 40.74, 59.81, 75.02, 91.01, 124.32))
lam = lamb
Nt = M

# just consider the first 6 ionization states
W = np.zeros([Nt, N_state])
lam = lamb
Nt = M
Z = np.arange(1, N_state+1, 1)             # the residual charge seen by this electron
N_pop = np.zeros([Nt, N_state])
N_pop[0, 0] = 1                                # initial population 
p = np.zeros(Nt)                  
y0=1
yj=0

# calculer the field 

# E_gau = np.abs(ini_gau)
E_gau = ga.gaussian(E0, t_pts, t_FWHM, t0)

# calculer the rate of ionization and population of state

    
W= st.rate(Z, omega0, U_i, E_gau, N_state, Nt)

N_pop = st.population(t_pts, W, N_state, Nt)  

# degree of the ionization and dz/dt
deg_z = st.degree(N_pop, t_pts)
der_dzdt = st.dzdt(deg_z, t_pts_fs)

# the gird point for dz/dt
t_pts_fs_der = t_pts_fs[:-1]          

# density of electron has a linear relation with dz/dt 
dens_electron = 3E8*Ni * deg_z             # turn into IS unit

#####################################################################
#                                                                   #
#                  propagation in plasma                            #
#                                                                   #
#####################################################################

# instantiation of the class properties  
pro_plasma = inter.inter(ini_gau, ini_dis,0 ,0 , L, T, N, M)

# make laser propagate 
pro_plasma.propagation(dens_electron)

# the grid point of frequency 
freq_plasma_pts = np.fft.fftfreq(M, dt)

# the frequency 
freq_plasma = sp.frequency(pro_plasma.e_right[:,N-1])
freq_plasma_abs = np.abs(freq_plasma)


print('dx = ', dx)               
print('dt = ', dt*1E15)
print('time = ', T*1E15)
print('length = ', L*1E6)
print('dfreq2 = ', freq_pts[1]- freq_pts[0])

######################################################################
# calculer the average dzdt
sumdzdt = 0
t_sum = 0

for i in range(M-1):
    if der_dzdt[i]>=1E-3:
        sumdzdt += der_dzdt[i]
        t_sum += 1

der_dzdt_average = sumdzdt/t_sum
   
#####################################################################
#the lambda in plasma

lamb_plasma_pts = np.zeros(1)
dis_plasma_pos = np.zeros(1)

lamb_plasma_pts = sp.fre_to_lam_pts(freq_plasma_pts, freq_plasma_abs)
lamb_plasma_pts_nm =lamb_plasma_pts*1E9

dis_plasma_pos = sp.fre_to_lam(freq_plasma_pts, freq_plasma_abs)
      
np.delete(lamb_plasma_pts, [0])
np.delete(dis_plasma_pos, [0])

#the lambda  in vacuum
freq_vacuum_abs = dis_freq_vacuum
freq_plasma_pts = freq_pts
lamb_vacuum_pts = np.zeros(1)
dis_vacuum_pos = np.zeros(1)

lamb_vacuum_pts = sp.fre_to_lam_pts(freq_plasma_pts, freq_vacuum_abs)
dis_vacuum_pos = sp.fre_to_lam(freq_plasma_pts, freq_vacuum_abs)
lamb_vacuum_pts_nm =lamb_vacuum_pts*1E9

np.delete(lamb_vacuum_pts, [0])
np.delete(dis_vacuum_pos, [0])

# the average shift
print(der_dzdt_average)
d_lam_ave = inter.delta_lamda_deg(L, lamb, Ni, der_dzdt_average)


new_lamb_pts = d_lam_ave + lamb_vacuum_pts
new_lamb_pts_nm = new_lamb_pts*1E9
print(new_lamb_pts)
print(len(new_lamb_pts))

