import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation

import data as da
import matplot as mat    
import gaussian as ga
# Physcial parameters 
pi = np.pi
c = 3E8                                    # the velocity of light (m*s^-1)
e = -1.60E-19                              # charge of electron (C)
m = 9.11E-31                               # mass of electron (kg)
epsilon0 = 8.85E-12                        # vacuum permittivity (F*m^−1) 
Ni = 1.34E26                               # densiy of ion (m^-3)
lamb = 620E-9                              # wavelength (m) 
f = c/lamb                                 # frequency
I0 = 1E20                                  # intensity (W/m^2)
E0  = ((1E20)*377)**0.5                    # the amplitude (V/m)
omega0 =(2*pi*c)/lamb                      # the angular frequency
t_FWHM = 100E-15                           # half of the W (s) = 100fs
t0= 200E-15                                # the center of the gaussian wacepacket (s)
n0 = 1                                     # fractive index 
v = c/n0
k0 = omega0/v

###################################################################
N = da.N
M =da.M
T = da.T
L = da.L
dt = da.dt
dx = da.dx
x_pts = da.x_pts
t_pts = da.t_pts
t_pts_fs = da.t_pts_fs
x_pts_um = da.x_pts_um
pro = da.pro
t_FWHM = da.t_FWHM
freq_pts = da.freq_pts
freq_abs = da.freq_abs
omega_pts = da.omega_pts

###################################################################
# figure of propagation 

fig = plt.figure(figsize=(15,25))
axes = plt.subplot(111)
axes.set_yticks([])
axes.plot(x_pts_um, pro.e[500,:], color='royalblue')
axes.plot(x_pts_um, pro.e[1000,:]+2.5E11, color='royalblue')
axes.plot(x_pts_um, pro.e[1500,:]+5E11, color='royalblue')
axes.plot(x_pts_um, pro.e[2000,:]+7.5E11, color='royalblue')
axes.plot(x_pts_um, pro.e[2500,:]+10E11, color='royalblue')
axes.set_xlim(0, L*1E6)
axes.set_title('Propagation along with x', fontsize=35, fontname ='Times New Roman')
axes.set_xlabel('Position($\mu m$)', fontsize=25, fontname ='Times New Roman')
axes.set_ylabel('Discrete time point', fontsize=25, fontname ='Times New Roman')
plt.savefig("propagation_with_time")
plt.legend()
plt.show()

####################################################################
# figure of a gaussian wavepacket
# Use the initial condition at the point x=0,  ini_gau = gaussian_wavepacket(E0, omega0, t_FWHM, t_pts, t0)

E_t = pro.e[:,0]                           # postion x=0
I_t = ((np.abs(E_t))**2 )/377 

# plot the ultrashort laser
fig, ax = plt.subplots(figsize=(14,9))
ax1, ax2 = mat.two_scales(ax, t_pts_fs, E_t, I_t, 'b', 'r', 'Gaussian temporal pulse', '$Electric\ field\ E(t)$', '$Time average intensity I(t)$', 'Time (fs)', 'Amplitude($V/m$)', 'Intensity(relative value)')
ax.set_xlim(0,400)
plt.legend('best')
plt.savefig("real_gaussian_temporal_pulse")
plt.show()

####################################################################
# Diagramme of a gaussian wavepacket
# Use the initial condition at the point x=0,  ini_gau = gaussian_wavepacket(E0, omega0, t_FWHM, t_pts, t0)

E_t = pro.e[:,0]

I_t = ga.gaussian(I0,t_pts,t_FWHM,t0)

# plot the ultrashort laser
fig, ax = plt.subplots(figsize=(14,9))
ax1, ax2 = mat.two_scales(ax, t_pts_fs,  E_t, I_t, 'b', 'r', 'Gaussian temporal pulse', '$Electric\ field\ E(t)$', '$Time average intensity I(t)$', 'Time (fs)', 'Amplitude($V/m$)', 'Intensity(relative value)')
ax.set_xlim(0,400)
plt.legend('best')
plt.savefig("real_gaussian_temporal_pulse_2")
plt.show()

####################################################################

####################################################################
# plot the spectrum of frequency 

fig, axes = plt.subplots(ncols=3, figsize=(20, 11))
ax = axes[0]
ax = mat.one_scale(ax, t_pts_fs, E_t,'royalblue', '-', 'Temporal pulse', 'E(t)', 'Time (fs)', 'Electric filed(V/m)')

ax = axes[1]
ax = mat.one_scale(ax, freq_pts, freq_abs, 'g', '-', 'Frequency spectrum', 'P(f)', 'Frequency f (Hz)', 'Itensity')
ax.set_xlim(-0.55E15,0.55E15)

ax = axes[2]
ax = mat.one_scale(ax, freq_pts, freq_abs, 'g', '-', 'Frequency spectrum(positive)', 'P(f)', 'Frequency f (Hz)', 'Intensity')
ax.set_xlim(0.47E15,0.50E15)

plt.savefig("frequency_spectrum")
plt.legend()
plt.show()

#####################################################################
# plot the spectrum of frequency omega

fig = plt.figure(figsize=(15,9))
ax = plt.subplot()
ax = mat.one_scale(ax, omega_pts, freq_abs,'b', '-', 'Frequency spectrum(positive)', 'P($\omega$)', 'Frequency $\omega$ (rad/s)', 'Intensity')
ax.set_xlim(0.25E16,0.35E16)
plt.savefig("frequency_omega_spectrum")
plt.legend()
plt.show()
   
#####################################################################  
