﻿import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation
import pylab as pl
from scipy import fftpack
###################################################################
import ammsov as am                        # ammosov()             ammosov()
import interaction as inter                # class inter()         delta_lamda_dens() delta_lamda_deg
import spectrum as sp                      # frequency()           fre_to_lam()       fre_to_lam_pts
import ode as ode                          # ode_0()               ode_j()
import gaussian as ga                      # gaussian_wavepacket() distribution()     average_intensity()     gaussian() 
import state as st                         # rate()                population()       degree()                dzdt()
import matplot as mat    

# Physcial parameters 
pi = np.pi
c = 3E8                                    # the velocity of light (m*s^-1)
e = -1.60E-19                              # charge of electron (C)
m = 9.11E-31                               # mass of electron (kg)
epsilon0 = 8.85E-12                        # vacuum permittivity (F*m^−1) 
Ni = 1.34E26                               # densiy of ion (m^-3)
lamb = 620E-9                              # wavelength (m) 
f = c/lamb                                 # frequency
I0 = 1E20                                  # intensity (W/m^2)
E0  = ((1E20)*377)**0.5                    # the amplitude (V/m)
omega0 =(2*pi*c)/lamb                      # the angular frequency
t_FWHM = 100E-15                           # half of the W (s) = 100fs
t0= 200E-15                                # the center of the gaussian wacepacket (s)
n0 = 1                                     # fractive index 
v = c/n0
k0 = omega0/v

#####################################################################
#                                                                   #
#                  propagation in vacuum                            #
#                                                                   #
#####################################################################

# the initial condition
v = c/n0                                  # don't consider fractive index 
L = 50E-6                                 # the length of interaction in one dimension  

N = 1000                                  
x_pts, dx = np.linspace(0, L, N, retstep=True)

dt = dx/c                                  # make sure that dt is about 1fs
M = 3000                                   # choose this number to make sure that a pulse pass through the length 
T = (M-1)*dt                               # the considering time 
t_pts = np.linspace(0, T, M)
dens_e0 = np.zeros(M)                      # electron density is zero in vacuum 

print('dx = ', dx)               
print('dt = ', dt*1E15)
print('time = ', T*1E15)
print('length = ', L*1E6)

# the gird position and time with the unit um and fs
t_pts_fs, dt_fs = np.linspace(0, T*1E15, M, retstep=True)
x_pts_um, dx_um = np.linspace(0, L*1E6, N, retstep=True)

# the  initial pulse and distribution
t_FWHM = 100E-15
x_FWHM = v*t_FWHM
t0 = 200E-15
x0 = -v*t0

ini_gau = ga.gaussian_wavepacket(E0, omega0, t_FWHM, t_pts, t0)
ini_dis = ga.distribution(E0, k0, x_FWHM, x_pts, x0)
ini_gau_l = ga.gaussian_wavepacket(E0, omega0, t_FWHM, t_pts, L/c+t0)
ini_dis_l = ga.distribution(E0, -k0, x_FWHM, x_pts, 2*L-x0)
#####################################################################
# instantiation of the class properties  
pro = inter.inter(ini_gau, ini_dis,0 ,0 , L, T, N, M)

# make laser propagate 
pro.propagation(dens_e0)


####################################################################
# frequency spectrum

# the grid point of frequency
Nt = M 
freq_pts = np.fft.fftfreq(Nt, dt)

# the frequency spectrum
dis_freq = sp.frequency(E_t)

freq_abs = np.abs(dis_freq)
freq_real = np.real(dis_freq)
freq_imag = np.imag(dis_freq)
dis_freq_vacuum =freq_abs
omega_pts = 2*pi*freq_pts

print('dfreq = ', freq_pts[1]- freq_pts[0])
####################################################################
####################################################################
# Ar element, 100um length of interaction, 1E16 W/cm^2 intensity 
N_state = 7
lam = lamb
Nt = M

# just consider the first 6 ionization states
U_i = np.array((15.76, 27.63, 40.74, 59.81, 75.02, 91.01, 124.32))
N = np.zeros([Nt, N_state])
Z = np.arange(1, N_state+1, 1)
W = np.zeros([Nt, N_state])

p = np.zeros(Nt)                  

# calculer the field 

# E_gau = np.abs(ini_gau)
E_gau = ga.gaussian(E0, t_pts, t_FWHM, t0)

# calculer the rate of ionization and population of state
W = st.rate(Z, omega0, U_i, E_gau, N_state, Nt)
N_pop = st.population(t_pts, W, N_state, Nt)  

# degree of the ionization and dz/dt
deg_z = st.degree(N, t_pts)
der_dzdt = st.dzdt(deg_z, t_pts_fs)

# the gird point for dz/dt
t_pts_fs_der = t_pts_fs[:-1]          

####################################################################
####################################################################

# figure of propagation 

fig = plt.figure(figsize=(15,25))
axes = plt.subplot(111)
axes.set_yticks([])
axes.plot(x_pts_um, pro.e[500,:], color='royalblue')
axes.plot(x_pts_um, pro.e[1000,:]+2.5E11, color='royalblue')
axes.plot(x_pts_um, pro.e[1500,:]+5E11, color='royalblue')
axes.plot(x_pts_um, pro.e[2000,:]+7.5E11, color='royalblue')
axes.plot(x_pts_um, pro.e[2500,:]+10E11, color='royalblue')
axes.set_xlim(0, L*1E6)
axes.set_title('Propagation along with x', fontsize=35, fontname ='Times New Roman')
axes.set_xlabel('Position($\mu m$)', fontsize=25, fontname ='Times New Roman')
axes.set_ylabel('Discrete time point', fontsize=25, fontname ='Times New Roman')
plt.savefig("propagation_with_time")
plt.legend()
plt.show()

####################################################################
# figure of a gaussian wavepacket
# Use the initial condition at the point x=0,  ini_gau = gaussian_wavepacket(E0, omega0, t_FWHM, t_pts, t0)

E_t = pro.e[:,0]                           # postion x=0
I_t = ((np.abs(E_t))**2 )/377 

# plot the ultrashort laser
fig, ax = plt.subplots(figsize=(14,9))
ax1, ax2 = mat.two_scales(ax, t_pts_fs,  ini_gau, I_t, 'b', 'r', 'Gaussian temporal pulse', '$Electric\ field\ E(t)$', '$Time average intensity I(t)$', 'Time (fs)', 'Amplitude($V/m$)', 'Intensity(relative value)')

plt.legend('best')
plt.savefig("real_gaussian_temporal_pulse")
plt.show()

####################################################################
# Diagramme of a gaussian wavepacket
# Use the initial condition at the point x=0,  ini_gau = gaussian_wavepacket(E0, omega0, t_FWHM, t_pts, t0)

E_t = pro.e[:,0]

I_t = gaussian(I0,t_pts,t_FWHM,t0)

# plot the ultrashort laser
fig, ax = plt.subplots(figsize=(14,9))
ax1, ax2 = mat.two_scales(ax, t_pts_fs,  ini_gau, I_t, 'b', 'r', 'Gaussian temporal pulse', '$Electric\ field\ E(t)$', '$Time average intensity I(t)$', 'Time (fs)', 'Amplitude($V/m$)', 'Intensity(relative value)')
ax1.set_xlim(0,400)
plt.legend('best')
plt.savefig("real_gaussian_temporal_pulse_2")
plt.show()

####################################################################
# propagation along with x

x_FWHM = v*t_FWHM
x0 =v*t0
Nx1 = 1000
xmin1, xmax1, = 0, 100E-6
x1_pts, dx1 = np.linspace(xmin1, xmax1, Nx1, retstep=True)
xmin1_um, xmax1_um, = 0, xmax1*1E6
x1_pts_um, dx1_um = np.linspace(xmin1_um, xmax1_um, Nx1, retstep=True)

# calculer the field 
E1_x = ga.distribution(E0, k0, x_FWHM, x_pts, x0)
I1_x = ((np.abs(E_x))**2 )/377           # feactor is to reach the 10^16W/cm^2

# plot the ultrashort laser
fig, ax = plt.subplots(figsize=(14,9))
ax1, ax2 = mat.two_scales(ax, x1_pts_um,  E1_x, I1_x, 'b', 'r', 'Gaussian spatial pulse', 'E(t)', 'I(t)','Position ($\mu m$)', 'Amplitude($V/m$)', 'Intensity($W/m^{2}$)')
ax1.set_xlim(0,L*1E6)

plt.legend('best')
plt.savefig("electric_field_and_intensity_space_spectrum")
plt.show()
####################################################################
# plot the spectrum of frequency 

fig, axes = plt.subplots(ncols=3, figsize=(20, 11))
ax = axes[0]
ax = mat.one_scale(ax, t_pts_fs, E_t,'royalblue', '-', 'Temporal pulse', 'E(t)', 'Time (fs)', 'Electric filed(V/m)')

ax = axes[1]
ax = mat.one_scale(ax, freq_pts, freq_abs, 'g', '-', 'Frequency spectrum', 'P(f)', 'Frequency f (Hz)', 'Itensity')
ax.set_xlim(-0.55E15,0.55E15)

ax = axes[2]
ax = mat.one_scale(ax, freq_pts, freq_abs, 'g', '-', 'Frequency spectrum(positive)', 'P(f)', 'Frequency f (Hz)', 'Intensity')
ax.set_xlim(0.47E15,0.50E15)

plt.savefig("frequency_spectrum")
plt.legend()
plt.show()

#####################################################################
# plot the spectrum of frequency omega

fig = plt.figure(figsize=(15,9))
ax = plt.subplot()
ax = mat.one_scale(ax, omega_pts, freq_abs,'b', '-', 'Frequency spectrum(positive)', 'P($\omega$)', 'Frequency $\omega$ (rad/s)', 'Intensity')
ax.set_xlim(0.25E16,0.35E16)
plt.savefig("frequency_omega_spectrum")
plt.legend()
plt.show()
   
#####################################################################  







#####################################################################
#                                                                   #
#                  propagation in plasma                            #
#                                                                   #
#####################################################################

# density of electron has a linear relation with dz/dt 
dens_electron = 3E8*Ni * deg_z

#####################################################################
#the electron density in plasma
fig =plt.figure(figsize=(15, 10))

ax= plt.subplot()
ax.plot(t_pts_fs, dens_electron,'g', lw=3, label='n(t)')
ax.set_xlabel('Time t (fs)', fontsize=28, fontname ='Times New Roman')
ax.set_ylabel('Electron density n(t) ($m^{-3}$)',  fontsize=28, fontname ='Times New Roman')
ax.set_title('Electron density n(t)', fontsize=35, fontname ='Times New Roman')
ax.set_xlim(0,400)
plt.savefig('current_in_plasma')
plt.legend()
plt.show()

#####################################################################
# the initial condition
n = 1                                      # don't consider fractive index 
v = c/n
L = 50E-6                                 # the length of interaction in one dimension  

N = 1000                                  
x_pts, dx = np.linspace(0, L, N, retstep=True)

dt = dx/c                                  # make sure that dt is about 1fs
M = 3000                                   # choose this number to make sure that a pulse pass through the length 
T = (M-1)*dt                               # the considering time 
t_pts = np.linspace(0, T, M)


# the gird position and time with the unit um and fs
t_pts_fs, dt_fs = np.linspace(0, T*1E15, M, retstep=True)
x_pts_um, dx_um = np.linspace(0, L*1E6, N, retstep=True)

# the  initial pulse and distribution
t_FWHM = 100E-15
x_FWHM = v*t_FWHM
t0 = 200E-15
x0 = -v*t0

ini_gau = gaussian_wavepacket(E0, omega0, t_FWHM, t_pts, t0)
ini_dis = distribution(E0, k0, x_FWHM, x_pts, x0)

#####################################################################

# instantiation of the class properties  
pro_plasma = inter(ini_gau, ini_dis,0 ,0 , L, T, N, M)

# make laser propagate 
pro_plasma.propagation(dens_electron)

# the grid point of frequency 
freq_pts = np.fft.fftfreq(M, dt)

print('dx = ', dx)
               
print('dt = ', dt*1E15)

print('time = ', T*1E15)

print('length = ', L)

print('dfreq2 = ', freq_pts[1]- freq_pts[0])
####################################################################
#the current in the center of plasma
fig =plt.figure(figsize=(15, 9))

ax= plt.subplot()
ax.plot(t_pts_fs, pro_plasma.j[:,0],'g',lw=2)
ax.set_xlabel('Time t (fs)', fontsize=28, fontname ='Times New Roman')
ax.set_ylabel('Current',  fontsize=28, fontname ='Times New Roman')
ax.set_title('Current in plasma', fontsize=35, fontname ='Times New Roman')
ax.set_xlim(0,400)
plt.savefig('current_in_plasma')
plt.legend()
plt.show()



####################################################################
# the frequency spectrum
freq_plasma = frequency(pro_plasma.e_right[:,N-1])
freq_plasma_abs = np.abs(freq_plasma)


# plot the spectrum of frequency 
fig, axes = plt.subplots(ncols=3, figsize=(20, 11))
ax = axes[0]
ax = one_scale(ax, t_pts_fs, pro_plasma.e_right[:,N-1],'royalblue', '-', 'Temporal pulse', 'E(t)', 'Time (fs)', 'Electric filed(V/m)')


ax = axes[1]
ax = one_scale(ax, freq_pts, freq_plasma_abs, 'g', '-', 'Frequency spectrum', 'P(f)', 'Frequency f (Hz)', 'Itensity')
ax.set_xlim(-0.55E15,0.55E15)

ax = axes[2]
ax = one_scale(ax, freq_pts, freq_plasma_abs, 'g', '-', 'Frequency spectrum(positive)', 'P(f)', 'Frequency f (Hz)', 'Intensity')
ax.set_xlim(0.40E15,0.60E15)

plt.savefig("spectrum_plasma")
plt.legend()
plt.show()

#################################################################

fig = plt.figure(figsize=(15, 9))

ax3 = plt.subplot()
ax3 = one_scale(ax3, 2*pi*freq_pts, freq_plasma_abs, 'g', '-', 'Frequency spectrum', 'line', 'Frequency spectrum $\omega$(rad/s)', 'Intensity')
ax3.set_xlim(2.5E15,3.5E15)
plt.savefig("frequency_spectrum_plasma")
plt.legend()
plt.show()


######################################################################
# calculer the average dzdt
sumdzdt = 0
t_sum = 0

for i in range(M-1):
    if der_dzdt[i]>=1E-3:
        sumdzdt += der_dzdt[i]
        t_sum += 1

der_dzdt_average = sumdzdt/t_sum
 
   
#####################################################################
#the lambda in plasma
lamb_pts = np.zeros(1)
freq_plasma_pos = np.zeros(1)

lamb_pts = fre_to_lam_pts(freq_pts, freq_plasma_abs)
freq_plasma_pos = fre_to_lam(freq_pts, freq_plasma_abs)


lamb_pts_nm =lamb_pts*1E9

      
np.delete(lamb_pts, [0])
np.delete(freq_plasma_pos, [0])


#the lambda  in vacuum
dis_freq = dis_freq_vacuum
lamb_pts1 = np.zeros(1)
freq_pos1 = np.zeros(1)
lamb_pts1 = fre_to_lam_pts(freq_pts, dis_freq)
freq_pos1 = fre_to_lam(freq_pts, dis_freq)
lamb_pts_nm1 =lamb_pts1*1E9
np.delete(lamb_pts1 , [0])
np.delete(freq_pos1, [0])


# the average shift
print(der_dzdt_average)
d_lam1 = delta_lamda_deg(L, lamb, Ni, der_dzdt_average)
print(d_lam1)

new_lamb_pts1 = d_lam1 +lamb_pts1
new_lamb_pts_nm1 = new_lamb_pts1*1E9
print(new_lamb_pts1)
print(len(new_lamb_pts1))

fig = plt.figure(figsize=(15, 9))
ax= plt.subplot()
ax.plot(lamb_pts_nm1, freq_pos1 , 'b', label = 'in vacuum')
ax.plot(lamb_pts_nm, freq_plasma_pos, 'g', label = 'shift in plasma')
#ax.plot(new_lamb_pts_nm1, freq_pos1, 'r', label = 'shift in palsma(by dZ/dt) ')
ax.set_xlabel('wavelength $\lambda(nm)$', fontsize=28, fontname ='Times New Roman')
ax.set_ylabel('Intensity',  fontsize=28, fontname ='Times New Roman')
ax.set_title('Wavelength in plasma', fontsize=35, fontname ='Times New Roman')
ax.set_xlim(500,640)
plt.savefig('wavelength_in_plasma')
plt.legend()
plt.show()

######################################################################
